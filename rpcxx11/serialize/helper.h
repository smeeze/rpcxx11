#pragma once

// system
#include <memory>

namespace rpcxx11 {
namespace serialize {
namespace helper {
template<typename T, typename type>
class Enum {
public:
  Enum(T & a) : _t(a) { }

  template<typename Archive>
  void serialize(Archive & ar) {
    type tmp(static_cast<type>(_t));
    ar & tmp;
    _t = static_cast<T>(tmp);
  }
private:
  T & _t;
};
}
}
}
