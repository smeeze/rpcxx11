#pragma once

// system
#include <type_traits>

namespace rpcxx11 {
namespace serialize {
class traits {
private:
  class Null {
  public:
    template<typename T> void operator&(const T &) { }
  };
public:
  //
  //
  //
  template<class ...> using void_t = void;
  //
  //
  //
  template< class, class = void_t<> >
  struct is_range : std::false_type { };
   
  template<class T>
  struct is_range<T, void_t<decltype(std::declval<T>().begin()), decltype(std::declval<T>().end())>> : std::true_type { };
  //
  //
  //
  template< class, class = void_t<> >
  struct has_serialize_member : std::false_type { };
   
  template<class T>
  struct has_serialize_member<T, void_t<decltype(std::declval<T>().serialize(std::declval<Null &>()))>> : std::true_type { };
  //
  //
  //
  template< class, class = void_t<> >
  struct has_no_marshal_member : std::true_type { };
   
  template<class T>
  struct has_no_marshal_member<T, void_t<decltype(std::declval<T>().marshal(std::declval<Null &>()))>> : std::false_type { };
  //
  //
  //
  template< class, class = void_t<> >
  struct has_marshal_member : std::false_type { };
   
  template<class T>
  struct has_marshal_member<T, void_t<decltype(std::declval<T>().marshal(std::declval<Null &>()))>> : std::true_type { };
  //
  //
  //
  template< class, class = void_t<> >
  struct has_no_unmarshal_member : std::true_type { };
   
  template<class T>
  struct has_no_unmarshal_member<T, void_t<decltype(std::declval<T>().unmarshal(std::declval<Null &>()))>> : std::false_type { };
  //
  //
  //
  template< class, class = void_t<> >
  struct has_unmarshal_member : std::false_type { };
   
  template<class T>
  struct has_unmarshal_member<T, void_t<decltype(std::declval<T>().unmarshal(std::declval<Null &>()))>> : std::true_type { };
};
}
}
