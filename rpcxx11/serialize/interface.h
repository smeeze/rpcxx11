#pragma once

// system
#include <cstdint>

namespace rpcxx11 {
namespace serialize {
template<typename delegate>
class interface {
public:
           interface(const delegate & d) : _delegate(d) { }
  virtual ~interface() { }

  const delegate & operator*() const { return _delegate; }

  // syntaxic sugar
  uint32_t get_offset() const { return _delegate.get_offset(); }
protected:
  delegate _delegate;
};
}
}
