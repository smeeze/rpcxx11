// system
#include <vector>
// rpcxx11
#include <rpcxx11/loader.h>
#include <rpcxx11/modules/asio/module.h>
#include <rpcxx11/modules/nanomsg/module.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  class storage {
  public:
    storage() {
      _modules.push_back(std::make_shared<rpcxx11::modules::asio::module>());
      _modules.push_back(std::make_shared<rpcxx11::modules::nanomsg::module>());
    }
    const std::vector<rpcxx11::module::pointer> & get_modules() const { return _modules; }
  private:
    std::vector<rpcxx11::module::pointer> _modules;
  };
}
///////////////////////////////////////////////////////////////////////////////
const std::vector<rpcxx11::module::pointer> &
rpcxx11::loader::get_modules() const {
  static storage storage_instance;
  return storage_instance.get_modules();
}
