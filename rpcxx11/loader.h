#pragma once

// system
#include <vector>
// rpcxx11
#include <rpcxx11/module.h>

namespace rpcxx11 {
class loader {
public:
  const std::vector<rpcxx11::module::pointer> & get_modules() const;
};
}
