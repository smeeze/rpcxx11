#pragma once

// system
#include <cstdint>
// rpcxx11
#include <rpcxx11/wire/handler.h>
#include <rpcxx11/wire/server.h>

namespace rpcxx11 {
namespace factory {
class wire {
public:
  static rpcxx11::wire::handler::pointer build_handler(const std::string & endpoint);
  static rpcxx11::wire::server::pointer  build_server(const std::string & endpoint, const rpcxx11::wire::handler::pointer & handler);
};
}
}
