#pragma once

// system
#include <cstring>
#include <iostream>
#include <type_traits>
#include <vector>
// boost
#include <boost/format.hpp>
// rpcxx11
#include <rpcxx11/delegate/interface.h>
#include <rpcxx11/exceptions/noSpace.h>

namespace rpcxx11 {
namespace delegate {
/**
 * \brief non-owning view on memory(vector, array, raw pointers)
 */
template<typename T>
class data_view : public rpcxx11::delegate::interface {
public:
  typedef typename std::add_const<T>::type T_const;

  data_view(T_const * b, T_const * e) : begin(b), end(e), current(const_cast<T *>(b)) { }
  data_view(const std::vector<T> &v) : data_view(v.data(), v.data() + v.size()) { }

  T_const * begin;
  T_const * end;
  T       * current;

  data_view<T>   operator+ (const uint32_t & size) const { return data_view<T>(*this) += size; }
  data_view<T> & operator+=(const uint32_t & size)       { current += size; return *this; }

  uint32_t get_offset() const override { return current - begin; }
  uint32_t get_available() const { return end - current; }

  void operator()(void * ptr, uint32_t size) override {
    _validate(size);

    std::memmove(ptr, current, size);
    current += size;
  }
  void operator()(const void * ptr, uint32_t size) override {
    _validate(size);

    std::memmove(current, ptr, size);
    current += size;
  }
private:
  void _validate(uint32_t size) const {
    if(size > get_available()) {
      throw(rpcxx11::exceptions::noSpace(boost::str(boost::format("buffer too small, needed: %i have: %i") % (size) % get_available())));
    }
  }
};
}
}

template<typename T>
inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::delegate::data_view<T> &dv) {
  out << "rpcxx11::delegate::data_view<T>(offset=" << dv.get_offset() << ", available=" << dv.get_available() << ")";
  return out;
}
