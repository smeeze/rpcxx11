#pragma once

// system
#include <cstdint>

namespace rpcxx11 {
namespace delegate {
class interface {
public:
  virtual ~interface() { }

  virtual uint32_t get_offset() const = 0;
  virtual void operator()(void * ptr, uint32_t size) = 0;
  virtual void operator()(const void * ptr, uint32_t size) = 0;
};
}
}
