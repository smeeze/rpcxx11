#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/delegate/interface.h>

namespace rpcxx11 {
namespace delegate {
template<typename Nested>
class debug : public rpcxx11::delegate::interface {
public:
  debug(const Nested& nest) : _nested(nest) { }

  uint32_t get_offset() const override {
    auto result(_nested.get_offset());
    std::cerr << __PRETTY_FUNCTION__ << ": " << result << std::endl;
    return result;
  }

  void operator()(void * ptr, uint32_t size) override {
    std::cerr << __PRETTY_FUNCTION__ << "(*, " << size << ")" << std::endl;
    _nested(ptr, size);
  }
  void operator()(const void * ptr, uint32_t size) override {
    std::cerr << __PRETTY_FUNCTION__ << "(*, " << size << ")" << std::endl;
    _nested(ptr, size);
  }
private:
  Nested _nested;
};
}
}

template<typename T>
inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::delegate::debug<T> &dv) {
  out << "rpcxx11::delegate::debug<T>(offset=" << dv.get_offset() << ")";
  return out;
}
