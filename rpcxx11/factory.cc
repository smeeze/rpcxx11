// system
#include <regex>
// boost
#include <boost/format.hpp>
#include <boost/lexical_cast.hpp>
// rpcxx11
#include <rpcxx11/exceptions/notFound.h>
#include <rpcxx11/factory.h>
#if 0
#include <rpcxx11/modules/nanomsg/client.h>
#include <rpcxx11/modules/nanomsg/server.h>
#include <rpcxx11/modules/asio/client.h>
#include <rpcxx11/modules/asio/server.h>
#include <rpcxx11/utils/uri.h>
#else
#include <rpcxx11/loader.h>
#endif
///////////////////////////////////////////////////////////////////////////////
namespace {
#if 0
  class _endpoint_dissect {
  public:
    enum class module { none, nano, asio };

    _endpoint_dissect(const std::string & endpoint) : _module(module::none) {
      rpcxx11::utils::uri uri(endpoint);

      uri.dump();

      if(uri.get_scheme() == "nano") { _module = module::nano; }
      if(uri.get_scheme() == "asio") { _module = module::asio; }

      std::string regex("([^:]+):(.*)");
      std::smatch matches;
      if(std::regex_match(uri.get_authority(), matches, std::regex(regex, std::regex::icase))) {
          _address=matches[1];
          _port=boost::lexical_cast<uint32_t>(matches[2]);
        } else {
          throw(rpcxx11::exceptions::notFound("could not dissect endpoint"));
        }

      if(_module==module::none) {
          throw(rpcxx11::exceptions::notFound("could not determine module from endpoint"));
        }

      std::cerr << "endpoint(address= " << get_address() << ", port= " << get_port() << ")" << std::endl;
    }

    const module      & get_module() { return _module; }
    const std::string & get_address() { return _address; }
    const uint32_t    & get_port() { return _port; }
  private:
    module      _module;
    std::string _address;
    uint32_t    _port;
  };
#else
  rpcxx11::loader & get_loader_instance() {
    static rpcxx11::loader loader_instance;
    return loader_instance;
  }
#endif
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::wire::handler::pointer
rpcxx11::factory::wire::build_handler(const std::string & endpoint) {
#if 0
  _endpoint_dissect ed(endpoint);
  switch(ed.get_module()) {
    case _endpoint_dissect::module::asio:
      return std::make_shared<rpcxx11::modules::asio::client>(ed.get_address(), ed.get_port());
      break;
    case _endpoint_dissect::module::nano:
      return std::make_shared<rpcxx11::modules::nanomsg::client>(boost::str(boost::format("tcp://%s:%i") % ed.get_address() %  ed.get_port()));
      break;
    default:
      break;
    }
#else
  for(const auto & m: get_loader_instance().get_modules()) {
    try {
      auto rv(m->build_handler(endpoint));
      if(rv!=nullptr) {
        return rv;
      }
    } catch(...) {
    }
  }
#endif
  throw(rpcxx11::exceptions::notFound("could not build handler"));
  return nullptr;
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::wire::server::pointer
rpcxx11::factory::wire::build_server(const std::string & endpoint, const rpcxx11::wire::handler::pointer & handler) {
#if 0
  _endpoint_dissect ed(endpoint);
  switch(ed.get_module()) {
    case _endpoint_dissect::module::asio:
      return std::make_shared<rpcxx11::modules::asio::server>(ed.get_address(), ed.get_port(), handler);
      break;
    case _endpoint_dissect::module::nano:
      return std::make_shared<rpcxx11::modules::nanomsg::server>(boost::str(boost::format("tcp://%s:%i") % ed.get_address() % ed.get_port()), handler);
      break;
    default:
      break;
    }
#else
  for(const auto & m: get_loader_instance().get_modules()) {
    try {
      auto rv(m->build_server(endpoint, handler));
      if(rv!=nullptr) {
        return rv;
      }
    } catch(...) {
    }
  }
#endif
  throw(rpcxx11::exceptions::notFound("could not build server"));
  return nullptr;
}
