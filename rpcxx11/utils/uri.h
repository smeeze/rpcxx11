#pragma once

// system
#include <ostream>
#include <map>

namespace rpcxx11 {
namespace utils {
/**
 * parses an rfc2396.txt URI
 */
class uri {
public:
  uri(const std::string &);
///////////////////////////////////////////////////////////////////////////////
  const std::string & get_scheme() const { return _scheme; }
  const std::string & get_authority() const { return _authority; }
  const std::string & get_path() const { return _path; }
  const std::string & get_query() const { return _query; }
  const std::string & get_fragment() const { return _fragment; }
///////////////////////////////////////////////////////////////////////////////
  rpcxx11::utils::uri set_scheme(const std::string &) const;
  rpcxx11::utils::uri set_authority(const std::string &) const;
  rpcxx11::utils::uri set_path(const std::string &) const;
  rpcxx11::utils::uri set_query(const std::string &) const;
  rpcxx11::utils::uri set_fragment(const std::string &) const;
///////////////////////////////////////////////////////////////////////////////
  std::string repr() const /* __attribute__ ((deprecated)) */;
  std::string get_sap() const;
///////////////////////////////////////////////////////////////////////////////
  rpcxx11::utils::uri                set_query_value(const std::string &, const std::string &) const;
  std::string                        get_query_value(const std::string &) const;
  std::map<std::string, std::string> get_query_map() const;
  rpcxx11::utils::uri                set_query(const std::map<std::string, std::string> &) const;
///////////////////////////////////////////////////////////////////////////////
  void dump() const;
private:
  std::string _id;
  std::string _scheme;
  std::string _authority;
  std::string _path;
  std::string _query;
  std::string _fragment;
};
}
}

inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::utils::uri &u) {
  out << "uri: " << u.repr();
  return out;
}

