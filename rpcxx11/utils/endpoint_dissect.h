#pragma once

// system
#include <cstdint>
#include <regex>
// boost
#include <boost/lexical_cast.hpp>
// rpcxx11
#include <rpcxx11/exceptions/notFound.h>
#include <rpcxx11/utils/uri.h>

namespace rpcxx11 {
namespace utils {
class endpoint_dissect {
public:
  endpoint_dissect(const std::string & endpoint) : _uri(endpoint) {
    _uri.dump();

    std::string regex("([^:]+):(.*)");
    std::smatch matches;
    if(std::regex_match(_uri.get_authority(), matches, std::regex(regex, std::regex::icase))) {
        _address=matches[1];
        _port=boost::lexical_cast<uint32_t>(matches[2]);
      } else {
        throw(rpcxx11::exceptions::notFound("could not dissect endpoint"));
      }

    std::cerr << "endpoint(address= " << get_address() << ", port= " << get_port() << ")" << std::endl;
  }

  const std::string & get_scheme() const { return _uri.get_scheme(); }
  const std::string & get_address() { return _address; }
  const uint32_t    & get_port() { return _port; }
private:
  rpcxx11::utils::uri _uri;
  std::string         _address;
  uint32_t            _port;
};
}
}
