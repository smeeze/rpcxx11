#pragma once

// system
#include <cstdint>
#include <algorithm>

namespace rpcxx11 {
namespace utils {
class swap {
public:
  static void in_place(uint16_t & v) { _in_place_2(reinterpret_cast<char*>(&v)); }
  static void in_place( int16_t & v) { _in_place_2(reinterpret_cast<char*>(&v)); }
  static void in_place(uint32_t & v) { _in_place_4(reinterpret_cast<char*>(&v)); }
  static void in_place( int32_t & v) { _in_place_4(reinterpret_cast<char*>(&v)); }
private:
  static void _in_place_2(char * arg) {
    std::swap(arg[0], arg[1]);
  }
  static void _in_place_4(char * arg) {
    std::swap(arg[0], arg[3]);
    std::swap(arg[1], arg[2]);
  }
  static void _in_place_8(char * arg) {
    std::swap(arg[0], arg[7]);
    std::swap(arg[1], arg[6]);
    std::swap(arg[2], arg[5]);
    std::swap(arg[3], arg[4]);
  }
};
}
}
