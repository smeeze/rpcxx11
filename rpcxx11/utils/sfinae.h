#pragma once

// system
#include <utility>

namespace rpcxx11 {
namespace utils {
namespace sfinae {
class if_exists {
private:
  // can be called, through reference
  template <typename T, typename... Args>
  static auto _call(T&& t, int, Args&&... args) -> decltype(void(std::forward<T>(t).call(std::forward<Args>(args)...))) {
    std::forward<T>(t).call(std::forward<Args>(args)...);
  }
  // can be called, through pointer
  template <typename T, typename... Args>
  static auto _call(T&& t, int, Args&&... args) -> decltype(void(std::forward<T>(t)->call(std::forward<Args>(args)...))) {
    std::forward<T>(t)->call(std::forward<Args>(args)...);
  }
  // can not be called
  template <typename T, typename... Args>
  static void _call(T&&, void*, Args&&...) {
  }
public:
  // entry
  template <typename T, typename... Args>
  static void call(T&& t, Args&&... args) {
    _call(std::forward<T>(t), 0, std::forward<Args>(args)...);
  }
};
}
}
}
