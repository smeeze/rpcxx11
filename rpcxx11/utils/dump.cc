// system
#include <cctype>
#include <iostream>
#include <string>
// rpcxx11
#include <rpcxx11/utils/dump.h>
///////////////////////////////////////////////////////////////////////////////
void
rpcxx11::utils::dump::as_hex(const uint8_t * ptr, uint32_t size) {
  uint32_t x(0);
  uint32_t n(size);
  const uint8_t * buf_ptr(ptr);

  std::string ascii;

  std::cerr << std::hex;
  while(n--) {
    if(!x) {
      std::cerr.fill('0');
      std::cerr.width(5);
      std::cerr << (int)(buf_ptr - ptr);
    }
    std::cerr << " "
              << (int)(((*buf_ptr) & 0xf0) >> 4)
              << (int)(((*buf_ptr) & 0x0f) >> 0)
              ;

    // ascii
    ascii += isprint(*buf_ptr) ? *buf_ptr : '.';

    // advance
    buf_ptr++;
    x++;
    if(x>=16) {
      x=0;
      std::cerr << " " << ascii << std::endl;
      ascii="";
    }
  }
  std::cerr << std::dec;

}
