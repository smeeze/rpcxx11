#pragma once

// system
#include <cstdint>
#include <vector>

namespace rpcxx11 {
namespace utils {
class dump {
public:
  static void as_hex(const std::vector<uint8_t> &b) { as_hex(b.data(), b.size()); }
  static void as_hex(const uint8_t *, uint32_t);
};
}
}
