// system
#include <iostream>
#include <string>
// boost
#include <boost/algorithm/string.hpp>
#include <boost/foreach.hpp>
#include <boost/format.hpp>
// rpcxx11
#include <rpcxx11/exceptions/notFound.h>
#include <rpcxx11/utils/uri.h>
#include <regex>
///////////////////////////////////////////////////////////////////////////////
/*
      ^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\?([^#]*))?(#(.*))?
       12            3  4          5       6  7        8 9

      scheme    = $2
      authority = $4
      path      = $5
      query     = $7
      fragment  = $9
*/
///////////////////////////////////////////////////////////////////////////////
rpcxx11::utils::uri::uri(const std::string & id) : _id(id) {
  std::string regex("^(([^:/?#]+):)?(//([^/?#]*))?([^?#]*)(\\?([^#]*))?(#(.*))?");
  //                  12            3  4          5       6   7        8 9

  std::smatch matches;
  if(std::regex_match(id, matches, std::regex(regex, /* std::regex::perl | */ std::regex::icase) /* , std::match_extra */)) {
    _scheme=matches[2];
    _authority=matches[4];
    _path=matches[5];
    _query=matches[7];
    _fragment=matches[9];
  }
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::utils::uri
rpcxx11::utils::uri::set_scheme(const std::string &p) const {
  rpcxx11::utils::uri rv(*this);
  rv._scheme=p;
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::utils::uri
rpcxx11::utils::uri::set_authority(const std::string &p) const {
  rpcxx11::utils::uri rv(*this);
  rv._authority=p;
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::utils::uri
rpcxx11::utils::uri::set_path(const std::string &p) const {
  rpcxx11::utils::uri rv(*this);
  rv._path=p;
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::utils::uri
rpcxx11::utils::uri::set_query(const std::string &p) const {
  rpcxx11::utils::uri rv(*this);
  rv._query=p;
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::utils::uri
rpcxx11::utils::uri::set_fragment(const std::string &p) const {
  rpcxx11::utils::uri rv(*this);
  rv._fragment=p;
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
std::string
rpcxx11::utils::uri::repr() const {
  std::string rv;
  if(get_scheme().size()) { rv+=get_scheme() + "://"; }
  if(get_authority().size()) { rv+=get_authority(); }
  if(get_path().size()) { rv+=get_path(); }
  if(get_query().size()) { rv+="?" + get_query(); }
  if(get_fragment().size()) { rv+="#" + get_fragment(); }
  return rv;
}
///////////////////////////////////////////////////////////////////////////////
std::string
rpcxx11::utils::uri::get_sap() const {
  auto n(_id.find_first_of("?#"));
  if(n==std::string::npos) {
    return _id;
  }

  return _id.substr(0, n);
  // return get_scheme() + get_authority() + get_path();
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::utils::uri
rpcxx11::utils::uri::set_query_value(const std::string &key, const std::string &value) const {
  std::map<std::string, std::string> m;
  // split
  std::vector<std::string> key_value_sequence;
  boost::split(key_value_sequence, get_query(), boost::is_any_of("&"));
  for(const std::string &key_value: key_value_sequence) {
    std::vector<std::string> split;
    boost::split(split, key_value, boost::is_any_of("="));
    if(split.size()==1) {
      m[split[0]]="";
    } else if(split.size()==2) {
      m[split[0]]=split[1];
    }
  }
  // add
  m[key]=value;
  std::string qv;
  typedef std::pair<std::string, std::string> string_pair;
  for(const string_pair &sp: m) {
    if(sp.first.size()) {
      if(qv.size()) {
        qv += "&" + sp.first + "=" + sp.second;
      } else {
        qv += "?" + sp.first + "=" + sp.second;
      }
    }
  }
  // return
  if(qv.size()) {
    return set_query(qv.substr(1));
  }

  return *this;
}
///////////////////////////////////////////////////////////////////////////////
std::string
rpcxx11::utils::uri::get_query_value(const std::string &key) const {
  std::vector<std::string> key_value_sequence;
  boost::split(key_value_sequence, get_query(), boost::is_any_of("&"));
  for(const std::string &key_value: key_value_sequence) {
    if(key_value == key) {
      return "";
    }
    std::vector<std::string> split;
    boost::split(split, key_value, boost::is_any_of("="));
    if(split.size()==2 && split[0]==key) {
      return split[1];
    }
  }

  throw(rpcxx11::exceptions::notFound(key));
}
///////////////////////////////////////////////////////////////////////////////
std::map<std::string, std::string>
rpcxx11::utils::uri::get_query_map() const {
  std::map<std::string, std::string> rv;

  std::vector<std::string> key_value_sequence;
  boost::split(key_value_sequence, get_query(), boost::is_any_of("&"));
  for(const std::string &key_value: key_value_sequence) {
    std::vector<std::string> split;
    boost::split(split, key_value, boost::is_any_of("="));
    if(split.size()==2) {
      rv[split[0]]=split[1];
    }
  }

  return rv;
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::utils::uri
rpcxx11::utils::uri::set_query(const std::map<std::string, std::string> &m) const {
  std::string s;
  for(const auto &i: m) {
    if(s.empty()) {
      s += boost::str(boost::format("%s=%s") % i.first % i.second);
    } else {
      s += boost::str(boost::format("&%s=%s") % i.first % i.second);
    }
  }
  return set_query(s);
}
///////////////////////////////////////////////////////////////////////////////
void
rpcxx11::utils::uri::dump() const {
  std::cerr << "  scheme:" << get_scheme() << std::endl;
  std::cerr << "  authority:" << get_authority() << std::endl;
  std::cerr << "  path:" << get_path() << std::endl;
  std::cerr << "  query:" << get_query() << std::endl;
  std::cerr << "  fragment:" << get_fragment() << std::endl;
}
