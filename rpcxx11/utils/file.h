#pragma once

// system
#include <cstdio>
#include <string>
// rpcxx11
#include <rpcxx11/exceptions/notFound.h>
#include <rpcxx11/exceptions/noSpace.h>

namespace rpcxx11 {
namespace utils {
class file {
public:
  enum class mode { read, write };

  static constexpr const char * mode_to_string(const mode &m) {
    return (m==mode::read) ? "rb" : "wb";
  }

  file(const file &) = delete;
  file & operator=(const file&) = delete;

  file(FILE * f) : _file(f) {
    if(_file == nullptr) {
      throw(rpcxx11::exceptions::notFound("could not open file"));
    }
  }
  file(const std::string & filename, const mode & m) : file(fopen(filename.c_str(), mode_to_string(m))) {
  }
  ~file() {
    if(_file != nullptr) {
      fclose(_file);
    }
  }
  void write(const void * ptr, uint32_t size) {
    if(fwrite(ptr, size, 1, _file) != 1) {
      throw(rpcxx11::exceptions::noSpace("could not write to file"));
    }
  }
  void read(void * ptr, uint32_t size) {
    if(fread(ptr, size, 1, _file) != 1) {
      throw(rpcxx11::exceptions::noSpace("could not read from file"));
    }
  }
private:
  FILE * _file;
};
}
}
