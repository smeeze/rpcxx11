#pragma once

// system
#include <string>

namespace rpcxx11 {
namespace utils {
class misc {
public:
  static std::string uuid();
};
}
}
