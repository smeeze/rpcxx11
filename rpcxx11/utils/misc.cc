// boost
#include <boost/uuid/uuid_generators.hpp>
#include <boost/uuid/uuid_io.hpp>
// rpcxx11
#include <rpcxx11/utils/misc.h>
///////////////////////////////////////////////////////////////////////////////
std::string
rpcxx11::utils::misc::uuid() {
  return boost::uuids::to_string(boost::uuids::random_generator()());
}
