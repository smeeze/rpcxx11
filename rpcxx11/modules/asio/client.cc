// system
#include <cstring>
#include <iostream>
// rpcxx11
#include <rpcxx11/exceptions/invalid.h>
#include <rpcxx11/modules/asio/client.h>
///////////////////////////////////////////////////////////////////////////////
rpcxx11::delegate::data_view<char>
rpcxx11::modules::asio::client::handle(const rpcxx11::delegate::data_view<char> & in, const rpcxx11::delegate::data_view<char> & out) {
  // result
  auto result(out);

  // send
  boost::asio::write(_socket, boost::asio::buffer(in.begin, in.get_offset()));

  // receive
  auto bytes_recv(_socket.read_some(boost::asio::buffer(result.current, result.get_available())));
  result+=bytes_recv;

  // return
  return result;
}
