
#pragma once

// rpcxx11
#include <rpcxx11/module.h>

namespace rpcxx11 {
namespace modules {
namespace asio {
class module : public rpcxx11::module {
public:
  rpcxx11::wire::handler::pointer build_handler(const std::string &) override;
  rpcxx11::wire::server::pointer  build_server(const std::string &, const rpcxx11::wire::handler::pointer &) override;
};
}
}
}
