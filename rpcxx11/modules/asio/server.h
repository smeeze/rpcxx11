#pragma once

// system
#include <memory>
#include <string>
#include <thread>
// boost
#include <boost/asio.hpp>
// rpcxx11
#include <rpcxx11/wire/handler.h>
#include <rpcxx11/wire/server.h>
#include <rpcxx11/modules/asio/session.h>
///////////////////////////////////////////////////////////////////////////////
namespace rpcxx11 {
namespace modules {
namespace asio {
class server : public rpcxx11::wire::server {
  class service {
  public:
    service(boost::asio::io_service& io_service, short port, rpcxx11::wire::handler::pointer handler) : _acceptor(io_service, boost::asio::ip::tcp::endpoint(boost::asio::ip::tcp::v4(), port)), _socket(io_service), _handler(handler) {
      do_accept();
    }
  private:
    void do_accept() {
      _acceptor.async_accept(_socket,
      [this](boost::system::error_code ec) {
        if (!ec) {
          std::make_shared<rpcxx11::modules::asio::session>(std::move(_socket), _handler)->start();
        }

        do_accept();
      });
    }
  private:
    boost::asio::ip::tcp::acceptor  _acceptor;
    boost::asio::ip::tcp::socket    _socket;
    rpcxx11::wire::handler::pointer _handler;
  };
public:
  server(const std::string &, uint16_t port, rpcxx11::wire::handler::pointer handler) : rpcxx11::wire::server(handler), _port(port) {
    _thread = std::thread(&server::_thread_function, this);
  }
  ~server() {
  }
private:
  std::thread                     _thread;
  uint16_t                        _port;
  boost::asio::io_service         _io_service;
private:
  void _thread_function() {
    service s(_io_service, _port, _handler);
    _io_service.run();
  }
};
};
}
}
