// rpcxx11
#include <rpcxx11/modules/asio/client.h>
#include <rpcxx11/modules/asio/module.h>
#include <rpcxx11/modules/asio/server.h>
#include <rpcxx11/utils/endpoint_dissect.h>
///////////////////////////////////////////////////////////////////////////////
rpcxx11::wire::handler::pointer
rpcxx11::modules::asio::module::build_handler(const std::string & endpoint) {
  rpcxx11::utils::endpoint_dissect e(endpoint);
  if(e.get_scheme() == "asio" ) {
    return std::make_shared<rpcxx11::modules::asio::client>(e.get_address(), e.get_port());
  }
  return nullptr;
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::wire::server::pointer
rpcxx11::modules::asio::module::build_server(const std::string & endpoint, const rpcxx11::wire::handler::pointer & handler) {
  rpcxx11::utils::endpoint_dissect e(endpoint);
  if(e.get_scheme() == "asio" ) {
    return std::make_shared<rpcxx11::modules::asio::server>(e.get_address(), e.get_port(), handler);
  }
  return nullptr;
}
