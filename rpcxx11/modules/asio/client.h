#pragma once

// system
#include <memory>
#include <thread>
// boost
#include <boost/asio.hpp>
// rpcxx11
#include <rpcxx11/wire/handler.h>

namespace rpcxx11 {
namespace modules {
namespace asio {
class client : public rpcxx11::wire::handler {
public:
  client(const std::string &host, uint16_t port) : _socket(_io_service), _resolver(_io_service) {
    boost::asio::connect(_socket, _resolver.resolve({host, std::to_string(port)}));
  }

  rpcxx11::delegate::data_view<char> handle(const rpcxx11::delegate::data_view<char> &, const rpcxx11::delegate::data_view<char> &) override;
private:
  boost::asio::io_service        _io_service;
  boost::asio::ip::tcp::socket   _socket;
  boost::asio::ip::tcp::resolver _resolver;
};
}
}
}
