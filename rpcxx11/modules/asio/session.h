#pragma once

// system
#include <cstdio>
#include <iostream>
#include <memory>
// boost
#include <boost/asio.hpp>
#include <boost/format.hpp>
// rpcxx11
#include <rpcxx11/wire/handler.h>
#include <rpcxx11/delegate/data_view.h>
///////////////////////////////////////////////////////////////////////////////
namespace rpcxx11 {
namespace modules {
namespace asio {
class session : public std::enable_shared_from_this<session> {
public:
  session(boost::asio::ip::tcp::socket socket, rpcxx11::wire::handler::pointer handler) : _vector_result(1e6), _vector_arguments(1e6), _socket(std::move(socket)), _handler(handler) {
    std::cerr << "session. remote=" << _socket.remote_endpoint().address().to_string() << std::endl;
  }
  ~session() {
    std::cerr << "~session. remote=" << _socket.remote_endpoint().address().to_string() << std::endl;
  }

  void start() {
    do_read();
  }
private:
  void do_read() {
    auto self(std::enable_shared_from_this<session>::shared_from_this());
    _socket.async_read_some(boost::asio::buffer(_vector_arguments), [this, self](boost::system::error_code ec, std::size_t length) {
      // std::cerr << __PRETTY_FUNCTION__ << " receiving " << length << " bytes" << std::endl;
      if (!ec) {
        try {
          rpcxx11::delegate::data_view<char> in(_vector_arguments.data(), _vector_arguments.data()+length);
          rpcxx11::delegate::data_view<char> out(_vector_result);
          auto result(_handler->handle(in, out));
          do_write(result.get_offset());
        } catch(const std::exception &e) {
          std::cerr << __PRETTY_FUNCTION__ << " error: " << e.what() << std::endl;
        } catch(...) {
          std::cerr << __PRETTY_FUNCTION__ << " error" << std::endl;
        }
      }
    });
  }

  void do_write(std::size_t length) {
    auto self(std::enable_shared_from_this<session>::shared_from_this());
    boost::asio::async_write(_socket, boost::asio::buffer(_vector_result, length),
    [this, self](boost::system::error_code ec, std::size_t /*length*/) {
      if (!ec) {
        do_read();
      }
    });
  }

  std::vector<char>               _vector_result;
  std::vector<char>               _vector_arguments;
  boost::asio::ip::tcp::socket    _socket;
  rpcxx11::wire::handler::pointer _handler;
};
}
}
}
