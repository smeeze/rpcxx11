#pragma once

// system
#include <memory>
#include <thread>
// rpcxx11
#include <rpcxx11/wire/handler.h>

namespace rpcxx11 {
namespace modules {
namespace nanomsg {
class client : public rpcxx11::wire::handler {
public:
  client(const std::string &);
  ~client();

  rpcxx11::delegate::data_view<char> handle(const rpcxx11::delegate::data_view<char> &, const rpcxx11::delegate::data_view<char> &) override;
private:
  int _socket_id;
  int _endpoint_id;
};
}
}
}
