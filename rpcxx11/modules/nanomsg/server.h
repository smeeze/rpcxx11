#pragma once

// system
#include <memory>
#include <thread>
// rpcxx11
#include <rpcxx11/wire/handler.h>
#include <rpcxx11/wire/server.h>

namespace rpcxx11 {
namespace modules {
namespace nanomsg {
class server : public rpcxx11::wire::server {
public:
  typedef std::shared_ptr<server> pointer;

  server(const std::string &, rpcxx11::wire::handler::pointer);
  ~server();
private:
  void _thread_function();
private:
  int                             _socket_id;
  int                             _endpoint_id;
  bool                            _thread_continue;
  std::thread                     _thread;
};
}
}
}
