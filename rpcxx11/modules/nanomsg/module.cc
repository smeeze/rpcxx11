// boost
#include <boost/format.hpp>
// rpcxx11
#include <rpcxx11/modules/nanomsg/client.h>
#include <rpcxx11/modules/nanomsg/module.h>
#include <rpcxx11/modules/nanomsg/server.h>
#include <rpcxx11/utils/endpoint_dissect.h>
///////////////////////////////////////////////////////////////////////////////
rpcxx11::wire::handler::pointer
rpcxx11::modules::nanomsg::module::build_handler(const std::string & endpoint) {
  rpcxx11::utils::endpoint_dissect e(endpoint);
  if(e.get_scheme() == "nano" ) {
    return std::make_shared<rpcxx11::modules::nanomsg::client>(boost::str(boost::format("tcp://%s:%i") % e.get_address() %  e.get_port()));
  }
  return nullptr;
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::wire::server::pointer
rpcxx11::modules::nanomsg::module::build_server(const std::string & endpoint, const rpcxx11::wire::handler::pointer & handler) {
  rpcxx11::utils::endpoint_dissect e(endpoint);
  if(e.get_scheme() == "nano" ) {
    return std::make_shared<rpcxx11::modules::nanomsg::server>(boost::str(boost::format("tcp://%s:%i") % e.get_address() % e.get_port()), handler);
  }
  return nullptr;
}
