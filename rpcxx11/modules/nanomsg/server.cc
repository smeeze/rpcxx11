// system
#include <iostream>
// nanomsg
#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>
// rpcxx11
#include <rpcxx11/exceptions/invalid.h>
#include <rpcxx11/modules/nanomsg/server.h>
///////////////////////////////////////////////////////////////////////////////
rpcxx11::modules::nanomsg::server::server(const std::string &address, rpcxx11::wire::handler::pointer handler) : rpcxx11::wire::server(handler) {
  _socket_id = nn_socket(AF_SP, NN_REP);
  if(_socket_id < 0) {
    throw(rpcxx11::exceptions::invalid("request/reply server socket id"));
  }
  _endpoint_id = nn_bind(_socket_id, address.c_str());
  if(_endpoint_id < 0) {
    throw(rpcxx11::exceptions::invalid("request/reply server endpoint id"));
  }

  _thread_continue = true;
  _thread = std::thread(&rpcxx11::modules::nanomsg::server::_thread_function, this);
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::modules::nanomsg::server::~server() {
  _thread_continue = false;
  nn_shutdown(_socket_id, _endpoint_id);
  nn_close(_socket_id);

  if(_thread.joinable()) {
    _thread.join();
  }
}
///////////////////////////////////////////////////////////////////////////////
void
rpcxx11::modules::nanomsg::server::_thread_function() {
  std::vector<char> read_buffer(1024 * 1024);
  std::vector<char> write_buffer(1024 * 1024);

  while(_thread_continue) {
    // process
    // rpcxx11::buffer::reader reader(read_buffer.data(), 0, bytes_recv);
    // rpcxx11::buffer::writer writer(write_buffer);
    try {
      // receive
      auto bytes_recv(nn_recv(_socket_id, read_buffer.data(), read_buffer.size(), 0));

      // handle
      rpcxx11::delegate::data_view<char> in(read_buffer.data(), read_buffer.data() + bytes_recv);
      rpcxx11::delegate::data_view<char> out(write_buffer);

      auto result(_handler->handle(in, out));

      // send
      nn_send(_socket_id, result.begin, result.get_offset(), 0);

    } catch(const std::exception &e) {
      std::cerr << "exception in " << __PRETTY_FUNCTION__ << ": " << e.what() << std::endl;
    } catch(...) {
      std::cerr << "unknown exception in " << __PRETTY_FUNCTION__ << std::endl;
    }
  }
}
