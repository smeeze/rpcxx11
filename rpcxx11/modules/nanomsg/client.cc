// system
#include <cstring>
#include <iostream>
// nanomsg
#include <nanomsg/nn.h>
#include <nanomsg/reqrep.h>
// rpcxx11
#include <rpcxx11/exceptions/invalid.h>
#include <rpcxx11/modules/nanomsg/client.h>
///////////////////////////////////////////////////////////////////////////////
rpcxx11::modules::nanomsg::client::client(const std::string &address) {
  _socket_id = nn_socket(AF_SP, NN_REQ);
  if(_socket_id < 0) {
    throw(rpcxx11::exceptions::invalid("request/reply  client socket id"));
  }
  _endpoint_id = nn_connect(_socket_id, address.c_str());
  if(_endpoint_id < 0) {
    throw(rpcxx11::exceptions::invalid("request/reply  client endpoint id"));
  }
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::modules::nanomsg::client::~client() {
  nn_shutdown(_socket_id, _endpoint_id);
  nn_close(_socket_id);
}
///////////////////////////////////////////////////////////////////////////////
rpcxx11::delegate::data_view<char>
rpcxx11::modules::nanomsg::client::handle(const rpcxx11::delegate::data_view<char> & in, const rpcxx11::delegate::data_view<char> & out) {
  // result
  auto result(out);

  // send
  nn_send(_socket_id, in.begin, in.get_offset(), 0);

  // receive
  auto bytes_recv(nn_recv(_socket_id, result.current, result.get_available(), 0));
  result+=bytes_recv;

  // return
  return result;
}
