#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/modules/CORBA/base.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
template<typename delegate>
class marshal : public rpcxx11::modules::CORBA::base<delegate> {
public:
  marshal(const delegate &d) : rpcxx11::modules::CORBA::base<delegate>(d) {
  }

  template<typename T>
  void operator&(const T & t) {
    _marshal(t, true);
  }
private:
  using rpcxx11::modules::CORBA::base<delegate>::_endian_fix;
  using rpcxx11::modules::CORBA::base<delegate>::_delegate;

  void _marshal(const char (&arg)[3], bool) { _delegate(&arg, 3); }
  void _marshal(const char (&arg)[4], bool) { _delegate(&arg, 4); }

  void _marshal(const  uint8_t & arg, bool) {                                                                                   _delegate(&arg, sizeof( uint8_t)); }
  void _marshal(const   int8_t & arg, bool) {                                                                                   _delegate(&arg, sizeof(  int8_t)); }
  void _marshal(const uint16_t & arg, bool) { auto a(arg); _endian_fix(a); base<delegate>::template _align<sizeof(uint16_t)>(); _delegate(&a, sizeof(uint16_t)); }
  void _marshal(const  int16_t & arg, bool) { auto a(arg); _endian_fix(a); base<delegate>::template _align<sizeof( int16_t)>(); _delegate(&a, sizeof( int16_t)); }
  void _marshal(const uint32_t & arg, bool) { auto a(arg); _endian_fix(a); base<delegate>::template _align<sizeof(uint32_t)>(); _delegate(&a, sizeof(uint32_t)); }
  void _marshal(const  int32_t & arg, bool) { auto a(arg); _endian_fix(a); base<delegate>::template _align<sizeof( int32_t)>(); _delegate(&a, sizeof( int32_t)); }
  void _marshal(const uint64_t & arg, bool) { auto a(arg); _endian_fix(a); base<delegate>::template _align<sizeof(uint64_t)>(); _delegate(&a, sizeof(uint64_t)); }
  void _marshal(const  int64_t & arg, bool) { auto a(arg); _endian_fix(a); base<delegate>::template _align<sizeof( int64_t)>(); _delegate(&a, sizeof( int64_t)); }

  // string
  void _marshal(const std::string & arg, bool) {
    uint32_t size(arg.size());
    _marshal(size, true);

    _delegate(arg.data(), size);
  }
  // vector
  template<typename T>
  void _marshal(const std::vector<T> & arg, bool) {
    uint32_t size(arg.size());
    _marshal(size, true);

    for(const auto &i: arg) { _marshal(i, true); }
  }

  // class with marshal member
  template<typename T>
  void _marshal(const T & t, decltype(std::is_member_function_pointer<decltype(&T::template marshal<rpcxx11::serialize::traits::Null>)>::value, bool())) {
    const_cast<T &>(t).marshal(*this);
  }
  // class with serializer member
  template<typename T>
  void _marshal(const T & t, decltype(std::is_member_function_pointer<decltype(&T::template serialize<rpcxx11::serialize::traits::Null>)>::value, bool())) {
    const_cast<T &>(t).serialize(*this);
  }
};
}
}
}
