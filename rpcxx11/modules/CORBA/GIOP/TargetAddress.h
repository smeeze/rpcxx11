#pragma once

// system
#include <cstdint>
#include <bitset>
// rpcxx11
#include <rpcxx11/exceptions/notFound.h>
#include <rpcxx11/modules/CORBA/GIOP/IORAddressingInfo.h>
#include <rpcxx11/modules/CORBA/IOP.h>
#include <rpcxx11/modules/CORBA/types.h>
#include <rpcxx11/serialize/helper.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
namespace GIOP {
class TargetAddress {
public:
  enum class AddressingDisposition : CORBA::UShort {
    Key, Profile, Reference
  };

  AddressingDisposition     addressing_disposition;
  OctetSequence             object_key;
  CORBA::IOP::TaggedProfile profile;
  IORAddressingInfo         ior;

  template<typename Archive>
  void serialize(Archive & ar) {
    rpcxx11::serialize::helper::Enum<AddressingDisposition, CORBA::UShort> addressing_disposition_helper(addressing_disposition);

    ar & addressing_disposition_helper;
    switch(addressing_disposition) {
    case AddressingDisposition::Key:
      ar & object_key;
      break;
    case AddressingDisposition::Profile:
      ar & profile;
      break;
    case AddressingDisposition::Reference:
      ar & ior;
      break;
    default:
      throw(rpcxx11::exceptions::notFound("AddressingDisposition"));
      break;
    }
  }

  std::string getKey() const {
    return std::string((char *)&object_key[0], object_key.size());
  }

  void setKey(const std::string &k) {
    addressing_disposition = AddressingDisposition::Key;
    object_key.assign(k.begin(), k.end());
  }
};
}
}
}
}

inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::modules::CORBA::GIOP::TargetAddress::AddressingDisposition &a) {
  switch(a) {
  case rpcxx11::modules::CORBA::GIOP::TargetAddress::AddressingDisposition::Key:
    out << "Key";
    break;
  case rpcxx11::modules::CORBA::GIOP::TargetAddress::AddressingDisposition::Profile:
    out << "Profile";
    break;
  case rpcxx11::modules::CORBA::GIOP::TargetAddress::AddressingDisposition::Reference:
    out << "Reference";
    break;
  }
  return out;
}

inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::modules::CORBA::GIOP::TargetAddress &a) {
  out << "TargetAddress("
      << "addressing_disposition=" << a.addressing_disposition
      << ", ";
  switch(a.addressing_disposition) {
  case rpcxx11::modules::CORBA::GIOP::TargetAddress::AddressingDisposition::Key:
    out << "key=" << a.getKey();
    break;
  case rpcxx11::modules::CORBA::GIOP::TargetAddress::AddressingDisposition::Profile:
    break;
  case rpcxx11::modules::CORBA::GIOP::TargetAddress::AddressingDisposition::Reference:
    break;
  }
  out << ")";
  return out;
}
