#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/modules/CORBA/types.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
namespace GIOP {
enum class MsgType_1_1 : CORBA::Octet {
  Request, Reply, CancelRequest, LocateRequest, LocateReply, CloseConnection, MessageError, Fragment
};
}
}
}
}

inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::modules::CORBA::GIOP::MsgType_1_1 &m) {
  switch(m) {
  case rpcxx11::modules::CORBA::GIOP::MsgType_1_1::Request:
    out << "Request";
    break;
  case rpcxx11::modules::CORBA::GIOP::MsgType_1_1::Reply:
    out << "Reply";
    break;
  case rpcxx11::modules::CORBA::GIOP::MsgType_1_1::CancelRequest:
    out << "CancelRequest";
    break;
  case rpcxx11::modules::CORBA::GIOP::MsgType_1_1::LocateRequest:
    out << "LocateRequest";
    break;
  case rpcxx11::modules::CORBA::GIOP::MsgType_1_1::LocateReply:
    out << "LocateReply";
    break;
  case rpcxx11::modules::CORBA::GIOP::MsgType_1_1::CloseConnection:
    out << "CloseConnection";
    break;
  case rpcxx11::modules::CORBA::GIOP::MsgType_1_1::MessageError:
    out << "MessageError";
    break;
  case rpcxx11::modules::CORBA::GIOP::MsgType_1_1::Fragment:
    out << "Fragment";
    break;
  default:
    out << "???";
    break;
  }
  return out;
}
