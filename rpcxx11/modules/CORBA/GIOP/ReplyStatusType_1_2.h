#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/modules/CORBA/types.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
namespace GIOP {
enum class ReplyStatusType_1_2 : CORBA::ULong {
  NO_EXCEPTION, USER_EXCEPTION, SYSTEM_EXCEPTION, LOCATION_FORWARD, LOCATION_FORWARD_PERM, NEEDS_ADDRESSING_MODE
};
}
}
}
}

inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::modules::CORBA::GIOP::ReplyStatusType_1_2 &s) {
  switch(s) {
  case rpcxx11::modules::CORBA::GIOP::ReplyStatusType_1_2::NO_EXCEPTION:
    out << "NO_EXCEPTION";
    break;
  case rpcxx11::modules::CORBA::GIOP::ReplyStatusType_1_2::USER_EXCEPTION:
    out << "USER_EXCEPTION";
    break;
  case rpcxx11::modules::CORBA::GIOP::ReplyStatusType_1_2::SYSTEM_EXCEPTION:
    out << "SYSTEM_EXCEPTION";
    break;
  case rpcxx11::modules::CORBA::GIOP::ReplyStatusType_1_2::LOCATION_FORWARD:
    out << "LOCATION_FORWARD";
    break;
  case rpcxx11::modules::CORBA::GIOP::ReplyStatusType_1_2::LOCATION_FORWARD_PERM:
    out << "LOCATION_FORWARD_PERM";
    break;
  case rpcxx11::modules::CORBA::GIOP::ReplyStatusType_1_2::NEEDS_ADDRESSING_MODE:
    out << "NEEDS_ADDRESSING_MODE";
    break;
  }
  return out;
}
