#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/modules/CORBA/types.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
namespace GIOP {

class Version {
public:
  Octet major;
  Octet minor;

  template<typename Archive>
  void serialize(Archive & ar) {
    ar & major;
    ar & minor;
  }
};
}
}
}
}
