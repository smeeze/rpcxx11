#pragma once

// system
#include <iostream>
#include <bitset>
// rpcxx11
#include <rpcxx11/modules/CORBA/types.h>
#include <rpcxx11/modules/CORBA/IOP.h>
#include <rpcxx11/modules/CORBA/GIOP/TargetAddress.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
namespace GIOP {

class RequestHeader_1_2 {
public:
  ULong                       request_id;
  Octet                       response_flags;
  char                        reserved[3];
  TargetAddress               target;
  std::string                 operation;
  IOP::ServiceContextSequence service_context_sequence;


  template<typename Archive>
  void serialize(Archive & ar) {
    ar & request_id;
    ar & response_flags;
    ar & reserved;
    ar & target;
    ar & operation;
    ar & service_context_sequence;
  }
};
}
}
}
}
inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::modules::CORBA::GIOP::RequestHeader_1_2 &h) {
  out << "GIOP::RequestHeader_1_2("
      << " request_id=" << h.request_id
      << " response_flags=" << std::bitset<8>(h.response_flags)
      << " target=" << h.target
      << " operation='" << h.operation << "'"
      << " service_context_sequence='" << h.service_context_sequence.size() << "'"
      << " )";
  return out;
}
