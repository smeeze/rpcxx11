#pragma once

// system
#include <iostream>
#include <bitset>
// boost
#include <boost/endian/conversion.hpp>
// rpcxx11
#include <rpcxx11/exceptions/invalid.h>
#include <rpcxx11/modules/CORBA/GIOP/MsgType_1_1.h>
#include <rpcxx11/modules/CORBA/GIOP/Version.h>
#include <rpcxx11/modules/CORBA/types.h>
#include <rpcxx11/serialize/helper.h>
#include <rpcxx11/utils/sfinae.h>
#include <rpcxx11/wire/byte_order.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
namespace GIOP {
class MessageHeader_1_1 {
  class Flags {
    template<typename T>
    class set_big_endian {
    public:
    
    };
  public:
    template<typename Archive>
    void marshal(Archive & ar) {
      if(BOOST_BYTE_ORDER=1234) { bitset[0] = 1; } else { bitset[0] = 0; }

      CORBA::Octet value(bitset.to_ulong());
      ar & value;
    }
    template<typename Archive>
    void unmarshal(Archive & ar) {
      CORBA::Octet value;
      ar & value;

      bitset = value;

      rpcxx11::utils::sfinae::if_exists::call(ar, bitset[0] == 0 ? rpcxx11::wire::byte_order::big : rpcxx11::wire::byte_order::little);
    }
    std::bitset<sizeof(CORBA::Octet)> bitset;
  };
public:
  char         magic[4];        // size  4   offset  0
  Version      version;         // size  2   offset  4
  Flags        flags;           // size  1   offset  6
  MsgType_1_1  message_type;    // size  1   offset  7
  CORBA::ULong message_size;    // size  4   offset  8
                                //           offset 12

  template<typename Archive>
  void serialize(Archive & ar) {
    rpcxx11::serialize::helper::Enum<MsgType_1_1, CORBA::Octet> message_type_helper(message_type);

    ar & magic;
    if(std::string(magic, 4) != "GIOP") {
      throw(rpcxx11::exceptions::invalid("not a GIOP header"));
    }
    ar & version;
    ar & flags;
    ar & message_type_helper;
    ar & message_size;
  }
};
}
}
}
}

inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::modules::CORBA::GIOP::MessageHeader_1_1 &m) {
  out << "GIOP::MessageHeader_1_1("
      << " magic=" << m.magic[0] << m.magic[1] << m.magic[2] << m.magic[3]
      << " version=" << (unsigned int)m.version.major << "." << (unsigned int)m.version.minor
      << " flags=" << m.flags.bitset
      << " type=" << m.message_type
      << " size=" << m.message_size
      << " )";
  return out;
}
