#pragma once

// system
#include <cstdint>
#include <bitset>
// rpcxx11
#include <rpcxx11/modules/CORBA/IOP.h>
#include <rpcxx11/modules/CORBA/types.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
namespace GIOP {

class IORAddressingInfo {
public:
  CORBA::ULong    selected_profile_index;
  CORBA::IOP::IOR ior;

  template<typename Archive>
  void serialize(Archive & ar) {
    ar & selected_profile_index;
    ar & ior;
  }
};
}
}
}
}
