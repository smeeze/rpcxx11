#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/modules/CORBA/GIOP/ReplyStatusType_1_2.h>
#include <rpcxx11/modules/CORBA/IOP.h>
#include <rpcxx11/modules/CORBA/types.h>
#include <rpcxx11/modules/interface.h>
#include <rpcxx11/modules/helper.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
namespace GIOP {
class ReplyHeader_1_2 {
public:
  ULong                       request_id;
  ReplyStatusType_1_2         reply_status;
  IOP::ServiceContextSequence service_context_sequence;

  template<typename Archive>
  void serialize(Archive & ar) {
    rpcxx11::modules::helper::Enum<ReplyStatusType_1_2> reply_status_helper(reply_status);

    ar & request_id;
    ar & reply_status_helper;
    ar & service_context_sequence;
  }
};
};
}
}
}

inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::modules::CORBA::GIOP::ReplyHeader_1_2 &h) {
  out << "GIOP::ReplyHeader_1_2("
      << std::endl
      << "  request_id=" << h.request_id
      << std::endl
      << "  reply_status=" << h.reply_status
      << std::endl
      << ")";
  return out;
}
