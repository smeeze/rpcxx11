#pragma once

// system
#include <iostream>
#include <typeinfo>
#include <vector>
// rpcxx11
#include <rpcxx11/modules/CORBA/base.h>
/*
template<typename T> std::string _dump_value(const T &) { return ""; }
template<>           std::string _dump_value(const  int16_t & arg) { return std::to_string(arg); }
template<>           std::string _dump_value(const uint16_t & arg) { return std::to_string(arg); }
template<>           std::string _dump_value(const  int32_t & arg) { return std::to_string(arg); }
template<>           std::string _dump_value(const uint32_t & arg) { return std::to_string(arg); }
template<>           std::string _dump_value(const std::string & arg) { return arg; }
*/
namespace rpcxx11 {
namespace modules {
namespace CORBA {
template<typename delegate>
class unmarshal : public rpcxx11::modules::CORBA::base<delegate> {
public:
  unmarshal(const delegate &d) : rpcxx11::modules::CORBA::base<delegate>(d) {
  }

#if 0
  class indent {
  public:
    indent() : _level(0) { }
    void inc() { _level += 2; }
    void dec() { _level -= 2; }

    std::string operator*() const { return std::string(' ', _level); }
  private:
    uint32_t _level;
  };
  class indent_guard {
  public:
    indent_guard(indent * i) : _indent(i) { _indent->inc(); }
    ~indent_guard() { _indent->dec(); }
  private:
    indent * _indent;
  };
  indent _indent;
#endif
  template<typename T>
  void operator&(T & arg) {
#if 0
    auto offset_before(_delegate.get_offset());
#endif
    _unmarshal(arg, true);
#if 0
    std::cerr << "----- CORBA unmarshal " << __PRETTY_FUNCTION__ << " -----"  << std::endl;
    std::cerr << "  offset=" << offset_before << std::endl;
    std::cerr << "  offset(next)=" << _delegate.get_offset() << std::endl;
    std::cerr << "  value=" << _dump_value(arg) << std::endl;
#endif
  }
private:
  using rpcxx11::modules::CORBA::base<delegate>::_endian_fix;
  using rpcxx11::modules::CORBA::base<delegate>::_delegate;

  void _unmarshal(char (&arg)[3], bool) { _delegate(&arg, 3); }
  void _unmarshal(char (&arg)[4], bool) { _delegate(&arg, 4); }
  void _unmarshal( uint8_t & arg, bool) {                                                      _delegate(&arg, sizeof(uint8_t)); }
  void _unmarshal(  int8_t & arg, bool) {                                                      _delegate(&arg, sizeof( int8_t)); }
  void _unmarshal(uint16_t & arg, bool) { base<delegate>::template _align<sizeof(uint16_t)>(); _delegate(&arg, sizeof(uint16_t)); _endian_fix(arg); }
  void _unmarshal( int16_t & arg, bool) { base<delegate>::template _align<sizeof( int16_t)>(); _delegate(&arg, sizeof( int16_t)); _endian_fix(arg); }
  void _unmarshal(uint32_t & arg, bool) { base<delegate>::template _align<sizeof(uint32_t)>(); _delegate(&arg, sizeof(uint32_t)); _endian_fix(arg); }
  void _unmarshal( int32_t & arg, bool) { base<delegate>::template _align<sizeof( int32_t)>(); _delegate(&arg, sizeof( int32_t)); _endian_fix(arg); }
  void _unmarshal(uint64_t & arg, bool) { base<delegate>::template _align<sizeof(uint64_t)>(); _delegate(&arg, sizeof(uint64_t)); _endian_fix(arg); }
  void _unmarshal( int64_t & arg, bool) { base<delegate>::template _align<sizeof( int64_t)>(); _delegate(&arg, sizeof( int64_t)); _endian_fix(arg); }

  // string
  void _unmarshal(std::string & arg, bool) {
    uint32_t size;
    _unmarshal(size, true);
#if 0
    std::cerr << __PRETTY_FUNCTION__ << std::endl;
    std::cerr << "  size=" << size << std::endl;
#endif
    if(size) {
      arg.assign(_delegate.current);
      _delegate += size;
    } else {
      arg.clear();
    }
  }
  // vector
  template<typename T>
  void _unmarshal(std::vector<T> & arg, bool) {
    uint32_t size;
    _unmarshal(size, true);
#if 0
    std::cerr << __PRETTY_FUNCTION__ << std::endl;
    std::cerr << "  size=" << size << std::endl;
#endif
    arg.resize(size);

    for(auto &i: arg) { _unmarshal(i, true); }
  }

  // class with unmarshal member
  template<typename T>
  void _unmarshal(T & t, decltype(std::is_member_function_pointer<decltype(&T::template unmarshal<rpcxx11::serialize::traits::Null>)>::value, bool())) {
    t.unmarshal(*this);
  }
  // class with serializer member
  template<typename T>
  void _unmarshal(T & t, decltype(std::is_member_function_pointer<decltype(&T::template serialize<rpcxx11::serialize::traits::Null>)>::value, bool())) {
    t.serialize(*this);
  }
};
}
}
}
