#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/serialize/interface.h>
#include <rpcxx11/serialize/traits.h>
#include <rpcxx11/modules/CORBA/marshal.h>
#include <rpcxx11/modules/CORBA/unmarshal.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
class serialize {
public:
  template<class Delegate> using marshal_type = rpcxx11::modules::CORBA::marshal<Delegate>;
  template<class Delegate> using unmarshal_type = rpcxx11::modules::CORBA::unmarshal<Delegate>;
};
}
}
}
