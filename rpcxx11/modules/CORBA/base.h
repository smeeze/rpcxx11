#pragma once

// rpcxx11
#include <rpcxx11/serialize/interface.h>
#include <rpcxx11/serialize/traits.h>
#include <rpcxx11/wire/byte_order.h>
// boost
#include <boost/endian/conversion.hpp>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
template<typename delegate>
class base : public rpcxx11::serialize::interface<delegate> {
public:
  base(const delegate &d) : rpcxx11::serialize::interface<delegate>(d) {
    call(BOOST_BYTE_ORDER==1234 ? rpcxx11::wire::byte_order::little : rpcxx11::wire::byte_order::big);
  }

  void call(const rpcxx11::wire::byte_order & bo) {
    _wire_byte_order = bo;
    // std::cerr << "wire byte order: " << _wire_byte_order << std::endl;
  }
protected:
  using rpcxx11::serialize::interface<delegate>::_delegate;

  void _endian_fix(uint16_t & arg) { if(_wire_byte_order==rpcxx11::wire::byte_order::big) { boost::endian::big_to_native_inplace(arg); } else { boost::endian::little_to_native_inplace(arg); } }
  void _endian_fix( int16_t & arg) { if(_wire_byte_order==rpcxx11::wire::byte_order::big) { boost::endian::big_to_native_inplace(arg); } else { boost::endian::little_to_native_inplace(arg); } }
  void _endian_fix(uint32_t & arg) { if(_wire_byte_order==rpcxx11::wire::byte_order::big) { boost::endian::big_to_native_inplace(arg); } else { boost::endian::little_to_native_inplace(arg); } }
  void _endian_fix( int32_t & arg) { if(_wire_byte_order==rpcxx11::wire::byte_order::big) { boost::endian::big_to_native_inplace(arg); } else { boost::endian::little_to_native_inplace(arg); } }
  void _endian_fix(uint64_t & arg) { if(_wire_byte_order==rpcxx11::wire::byte_order::big) { boost::endian::big_to_native_inplace(arg); } else { boost::endian::little_to_native_inplace(arg); } }
  void _endian_fix( int64_t & arg) { if(_wire_byte_order==rpcxx11::wire::byte_order::big) { boost::endian::big_to_native_inplace(arg); } else { boost::endian::little_to_native_inplace(arg); } }
/*
  uint32_t _endian_fix(const uint32_t & arg) { if(_wire_byte_order==rpcxx11::wire::byte_order::big) { return boost::endian::big_to_native(arg); } else { boost::endian::little_to_native(arg); } }
   int32_t _endian_fix(const  int32_t & arg) { if(_wire_byte_order==rpcxx11::wire::byte_order::big) { return boost::endian::big_to_native(arg); } else { boost::endian::little_to_native(arg); } }
*/
  template<uint32_t N>
  void _align() {
    uint32_t mod(_delegate.get_offset() % N);
    if(mod) {
      uint32_t cor(N - mod);
#if 0
      std::cerr << __PRETTY_FUNCTION__
                << " offset=" << _delegate.get_offset()
                << " mod=" << mod
                << " cor=" << cor
                << " offset(corrected)=" << _delegate.get_offset()+cor
                << std::endl;
#endif
      _delegate+=cor;
    }
  }
protected:
  rpcxx11::wire::byte_order _wire_byte_order;
};
}
}
}
