#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/modules/CORBA/types.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
class IOP {
public:
  typedef CORBA::ULong         ProfileId;
  typedef CORBA::OctetSequence ProfileData;

  class TaggedProfile {
  public:
    ProfileId   tag;
    ProfileData profile_data;

    template<typename Archive>
    void serialize(Archive & ar) {
      ar & tag;
      ar & profile_data;
    }
  };

  typedef std::vector<TaggedProfile> TaggedProfileSequence;

  typedef CORBA::ULong         ServiceId;
  typedef CORBA::OctetSequence ServiceData;

  class ServiceContext {
  public:
    ServiceId   service_id;
    ServiceData service_data;

    template<typename Archive>
    void serialize(Archive & ar) {
      ar & service_id;
      ar & service_data;
    }
  };

  class ServiceContextSequence : public std::vector<ServiceContext> {
  public:
    template<typename Archive>
    void serialize(Archive & ar) {
      uint32_t s = size();
      ar & s;

      if(s) {
        resize(s);
      } else {
        clear();
      }
    }
  };

  class IOR {
  public:
    std::string           type_id;
    TaggedProfileSequence profiles;

    template<typename Archive>
    void serialize(Archive &) {
      std::cerr << __PRETTY_FUNCTION__ << std::endl;
    }
  };
};
}
}
}
