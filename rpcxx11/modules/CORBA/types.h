#pragma once

// system
#include <cstdint>
#include <iostream>
#include <vector>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
typedef uint8_t  Octet;
typedef uint16_t UShort;
typedef  int16_t Short;
typedef uint32_t ULong;
typedef  int32_t Long;

typedef std::vector<Octet> OctetSequence;
}
}
}
