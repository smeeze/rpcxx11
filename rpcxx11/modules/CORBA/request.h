#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/modules/CORBA/types.h>
#include <rpcxx11/modules/CORBA/GIOP/MessageHeader_1_1.h>
#include <rpcxx11/modules/CORBA/GIOP/RequestHeader_1_2.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
class request {
public:
  template<typename Archive>
  void serialize(Archive & ar) {
    ar & giop_message_header;
    ar & giop_request_header;
    // ar & body_size;
  }

   CORBA::GIOP::MessageHeader_1_1 giop_message_header;
   CORBA::GIOP::RequestHeader_1_2 giop_request_header;
   // CORBA::ULong                   body_size;
};
}
}
}

inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::modules::CORBA::request &r) {
  out << "corba::request("
      << std::endl
      << "  MessageHeader=" << r.giop_message_header
      << std::endl
      << "  RequestHeader=" << r.giop_request_header
      << std::endl
      // << "  BodySize=" << r.body_size
      // << std::endl
      << ")"
      ;
  return out;
}
