#pragma once

// system
#include <string>
// boost
#include <boost/format.hpp>
// rpcxx11
#include <rpcxx11/delegate/data_view.h>
#include <rpcxx11/modules/CORBA/request.h>
#include <rpcxx11/modules/CORBA/marshal.h>
#include <rpcxx11/modules/CORBA/unmarshal.h>

namespace rpcxx11 {
namespace modules {
namespace CORBA {
class session {
public:
  typedef uint32_t                                          servant_id_type;
  typedef std::string                                       function_id_type;
  typedef rpcxx11::delegate::data_view<char>                delegate_type;
  typedef rpcxx11::modules::CORBA::marshal<delegate_type>   marshal_type;
  typedef rpcxx11::modules::CORBA::unmarshal<delegate_type> unmarshal_type;

  template<typename... Args>
  session(const Args & ...args) {
    std::cerr << __PRETTY_FUNCTION__ << std::endl;
  }

  class header_type {
  public:
    servant_id_type servant_id;
    function_id_type function_id;
  };
/*
  class marshal_base {
  public:
    template<typename T>
    void operator&(T & t) {
      std::cerr << __PRETTY_FUNCTION__ << std::endl;
    }

    uint32_t get_offset() const {
      std::cerr << __PRETTY_FUNCTION__ << std::endl;
      return 0;
    }
  };
  class marshal_type : public marshal_base {
  public:
  };
  class unmarshal_type : public marshal_base {
  public:
  };
*/
  class client_type {
  public:
  };

  //
  // servant
  //
  class servant_transaction {
  public:
    header_type                      header;
    rpcxx11::modules::CORBA::request request;
    unmarshal_type                   unmarshal;
    marshal_type                     marshal;
  };

  static servant_transaction build_servant_transaction(const delegate_type & unmarshal_delegate, const delegate_type & marshal_delegate) {
    header_type                      header;
    rpcxx11::modules::CORBA::request request;

    unmarshal_type unmarshal(unmarshal_delegate);
    unmarshal & request;

    std::cerr << __PRETTY_FUNCTION__ << std::endl;
    std::cerr << request << std::endl;

    return servant_transaction{header, request, *unmarshal, marshal_type(marshal_delegate)};
  }

  //
  // proxy
  //
  class proxy_transaction {
  public:
    marshal_type   marshal;
    unmarshal_type unmarshal;

    void commit() {
      std::cerr << __PRETTY_FUNCTION__ << std::endl;
    }
  };
/*
  proxy_transaction build_proxy_transaction(servant_id_type servant_id, function_id_type function_id) {
    std::cerr << __PRETTY_FUNCTION__ << "(" << servant_id << ", " << function_id << ")" << std::endl;
    proxy_transaction result;
    return result;
  }
*/
  proxy_transaction build_proxy_transaction(servant_id_type /* servant_id */, function_id_type /* function_id */) {
    _marshal_buffer.resize(1024);
    _unmarshal_buffer.resize(1024);

    rpcxx11::delegate::data_view<char> marshal_data_view(_marshal_buffer);
    rpcxx11::delegate::data_view<char> unmarshal_data_view(_unmarshal_buffer);

    delegate_type                      marshal_delegate(marshal_data_view);
    delegate_type                      unmarshal_delegate(unmarshal_data_view);
#if 0
    marshal_type                       marshal(marshal_delegate);
    
    header_type header{servant_id, function_id};
    header & marshal;
#endif
    return proxy_transaction{marshal_delegate, unmarshal_delegate};
  }

  template<typename T>
  static
  function_id_type gen_function_id(const T & t);
  static
  function_id_type gen_function_id(const int & i) { return boost::str(boost::format("func%i") % i); }
private:
  std::vector<char> _marshal_buffer;
  std::vector<char> _unmarshal_buffer;
};
}
}
}
