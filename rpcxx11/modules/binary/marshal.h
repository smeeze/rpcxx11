#pragma once

// system
#include <iostream>
#include <vector>
// rpcxx11
#include <rpcxx11/serialize/interface.h>
#include <rpcxx11/serialize/traits.h>

namespace rpcxx11 {
namespace modules {
namespace binary {
template<typename delegate>
class marshal : public rpcxx11::serialize::interface<delegate> {
public:
  marshal(const delegate &d) : rpcxx11::serialize::interface<delegate>(d) {
  }

  template<typename T>
  void operator&(const T & t) {
    _marshal(t);
  }
private:
  using rpcxx11::serialize::interface<delegate>::_delegate;

  // plain old data
  template<class T, typename std::enable_if<std::is_pod<T>::value, int>::type = 0>
  void _marshal(const T & t) { _delegate(&t, sizeof(t)); }
  // range
  template<class T, typename std::enable_if<rpcxx11::serialize::traits::is_range<T>::value, int>::type = 0>
  void _marshal(const T & t) {
    int32_t size(std::distance(std::begin(t), std::end(t)));
    _marshal(size);

    for(const auto & v: t) {
      _marshal(v);
    }
  }
  // serialize member
  template<class T,
           typename std::enable_if<serialize::traits::has_serialize_member<T>::value, int>::type = 0,
           typename std::enable_if<serialize::traits::has_no_marshal_member<T>::value, int>::type = 0>
  void _marshal(const T & t) {
    const_cast<T &>(t).serialize(*this);
  }
  // marshal member
  template<class T, typename std::enable_if<serialize::traits::has_marshal_member<T>::value, int>::type = 0>
  void _marshal(const T & t) {
    const_cast<T &>(t).marshal(*this);
  }
};
}  
}
}
