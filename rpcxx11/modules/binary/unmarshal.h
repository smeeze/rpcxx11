#pragma once

// system
#include <iostream>
#include <vector>
// rpcxx11
#include <rpcxx11/serialize/interface.h>
#include <rpcxx11/serialize/traits.h>

namespace rpcxx11 {
namespace modules {
namespace binary {
template<typename delegate>
class unmarshal : public rpcxx11::serialize::interface<delegate> {
public:
  unmarshal(const delegate &d) : rpcxx11::serialize::interface<delegate>(d) {
  }

  template<typename T>
  void operator&(T & t) {
    _unmarshal(t);
  }
private:
  using rpcxx11::serialize::interface<delegate>::_delegate;

  // plain old data
  template<class T, typename std::enable_if<std::is_pod<T>::value, int>::type = 0>
  void _unmarshal(T & t) { _delegate(&t, sizeof(t)); }
  // range
  template<class T, typename std::enable_if<serialize::traits::is_range<T>::value, int>::type = 0>
  void _unmarshal(T & t) {
    int32_t size;
    _unmarshal(size);

    t.resize(size);

    for(auto & v: t) {
      _unmarshal(v);
    }
  }
  // serialize member
  template<class T,
           typename std::enable_if<serialize::traits::has_serialize_member<T>::value, int>::type = 0,
           typename std::enable_if<serialize::traits::has_no_unmarshal_member<T>::value, int>::type = 0>
  void _unmarshal(T & t) {
    t.serialize(*this);
  }
  // unmarshal member
  template<class T, typename std::enable_if<serialize::traits::has_unmarshal_member<T>::value, int>::type = 0>
  void _unmarshal(T & t) {
    t.unmarshal(*this);
  }
};
}  
}
}
