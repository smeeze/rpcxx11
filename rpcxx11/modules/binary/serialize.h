#pragma once

// system
#include <iostream>
// rpcxx11
#include <rpcxx11/serialize/interface.h>
#include <rpcxx11/serialize/traits.h>
#include <rpcxx11/modules/binary/marshal.h>
#include <rpcxx11/modules/binary/unmarshal.h>

namespace rpcxx11 {
namespace modules {
namespace binary {
class serialize {
public:
  template<class Delegate> using marshal_type = rpcxx11::modules::binary::marshal<Delegate>;
  template<class Delegate> using unmarshal_type = rpcxx11::modules::binary::unmarshal<Delegate>;
};
}
}
}
