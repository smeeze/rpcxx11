#pragma once

// system
#include <string>
// boost
#include <boost/format.hpp>
// rpcxx11
#include <rpcxx11/delegate/data_view.h>

namespace rpcxx11 {
namespace modules {
namespace debug {
class session {
public:
  typedef uint32_t                           servant_id_type;
  typedef std::string                        function_id_type;
  typedef rpcxx11::delegate::data_view<char> delegate_type;

  template<typename... Args>
  session(const Args & ...args) {
    std::cerr << __PRETTY_FUNCTION__ << std::endl;
  }

  class header_type {
  public:
    servant_id_type servant_id;
    function_id_type function_id;
  };

  class marshal_base {
  public:
    template<typename T>
    void operator&(T & t) {
      std::cerr << __PRETTY_FUNCTION__ << std::endl;
    }

    uint32_t get_offset() const {
      std::cerr << __PRETTY_FUNCTION__ << std::endl;
      return 0;
    }
  };
  class marshal_type : public marshal_base {
  public:
  };
  class unmarshal_type : public marshal_base {
  public:
  };
  class client_type {
  public:
  };

  //
  // servant
  //
  class servant_transaction {
  public:
    header_type    header;
    unmarshal_type unmarshal;
    marshal_type   marshal;
  };

  static servant_transaction build_servant_transaction(const delegate_type & /* unmarshal_delegate */, const delegate_type & /* marshal_delegate */) {
    std::cerr << __PRETTY_FUNCTION__ << std::endl;
    servant_transaction result;
    return result;
  }

  //
  // proxy
  //
  class proxy_transaction {
  public:
    marshal_type   marshal;
    unmarshal_type unmarshal;

    void commit() {
      std::cerr << __PRETTY_FUNCTION__ << std::endl;
    }
  };

  proxy_transaction build_proxy_transaction(servant_id_type servant_id, function_id_type function_id) {
    std::cerr << __PRETTY_FUNCTION__ << "(" << servant_id << ", " << function_id << ")" << std::endl;
    proxy_transaction result;
    return result;
  }

  template<typename T>
  static
  function_id_type gen_function_id(const T & t);
  static
  function_id_type gen_function_id(const int & i) { return boost::str(boost::format("func%i") % i); }
};
}
}
}
