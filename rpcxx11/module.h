#pragma once

// system
#include <memory>
// rpcxx11
#include <rpcxx11/wire/handler.h>
#include <rpcxx11/wire/server.h>

namespace rpcxx11 {
class module {
public:
  using pointer = std::shared_ptr<module>;

  virtual ~module() { }

  virtual rpcxx11::wire::handler::pointer build_handler(const std::string &) { return nullptr; }
  virtual rpcxx11::wire::server::pointer  build_server(const std::string &, const rpcxx11::wire::handler::pointer &) { return nullptr; }
};
}
