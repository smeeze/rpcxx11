#pragma once

// system
#include <string>
#include <exception>

namespace rpcxx11 {
namespace exceptions {
class notFound : public std::exception {
public:
  notFound(const std::string & msg) : _msg(std::string(__FUNCTION__) + ": " + msg) {
  }
  virtual ~notFound() throw() {
  }
  virtual const char * what() const throw() override {
    return _msg.c_str();
  }
private:
  std::string _msg;
};
}
}
