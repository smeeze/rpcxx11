#pragma once

// system
#include <string>
#include <exception>

namespace rpcxx11 {
namespace exceptions {
class invalid : public std::exception {
public:
  invalid(const std::string & msg) : _msg(std::string(__FUNCTION__) + ": " + msg) {
  }
  virtual ~invalid() throw() {
  }
  virtual const char * what() const throw() override {
    return _msg.c_str();
  }
private:
  std::string _msg;
};
}
}
