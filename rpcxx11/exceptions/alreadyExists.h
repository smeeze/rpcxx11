#pragma once

// system
#include <string>
#include <exception>

namespace rpcxx11 {
namespace exceptions {
class alreadyExists : public std::exception {
public:
  alreadyExists(const std::string & msg) : _msg(std::string(__FUNCTION__) + ": " + msg) {
  }
  virtual ~alreadyExists() throw() {
  }
  virtual const char * what() const override throw() {
    return _msg.c_str();
  }
private:
  std::string _msg;
};
}
}
