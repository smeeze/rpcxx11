#pragma once

// system
#include <string>
#include <exception>

namespace rpcxx11 {
namespace exceptions {
class noSpace : public std::exception {
public:
  noSpace(const std::string & msg) : _msg(std::string(__FUNCTION__) + ": " + msg) {
  }
  virtual ~noSpace() throw() {
  }
  virtual const char * what() const throw() override {
    return _msg.c_str();
  }
private:
  std::string _msg;
};
}
}
