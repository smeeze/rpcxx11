#pragma once

// system
#include <iostream>

namespace rpcxx11 {
namespace wire {
enum class byte_order {
  big, little
};
}
}

inline
std::ostream& operator<<(std::ostream &out, const rpcxx11::wire::byte_order & bo) {
  out << "rpcxx11::wire::byte_order(";
  switch(bo) {
    case rpcxx11::wire::byte_order::big:
      out << "big";
      break;
    case rpcxx11::wire::byte_order::little:
      out << "little";
      break;
  }
  out << ")";
  return out;
}
