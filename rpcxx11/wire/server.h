#pragma once

// system
#include <memory>
// rpcxx11
#include <rpcxx11/wire/handler.h>

namespace rpcxx11 {
namespace wire {
class server {
public:
  typedef std::shared_ptr<server> pointer;

  server(rpcxx11::wire::handler::pointer handler) : _handler(handler) { }

  virtual ~server() { }
protected:
  rpcxx11::wire::handler::pointer _handler;
};
}
}
