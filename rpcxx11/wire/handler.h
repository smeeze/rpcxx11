#pragma once

// system
#include <memory>
// rpcxx11
#include <rpcxx11/delegate/data_view.h>

namespace rpcxx11 {
namespace wire {
class handler {
public:
  typedef std::shared_ptr<handler> pointer;

  virtual ~handler() { }

  virtual rpcxx11::delegate::data_view<char> handle(const rpcxx11::delegate::data_view<char> &, const rpcxx11::delegate::data_view<char> &) = 0;
};
}
}
