#pragma once

// boost
#include <boost/bind.hpp>
#include <boost/config/warning_disable.hpp>
#define BOOST_SPIRIT_DEBUG
#include <boost/spirit/include/qi.hpp>
#include <boost/spirit/include/phoenix.hpp>

namespace sandbox {
namespace silly {
template <typename Iterator, typename Skip>
class grammar: public boost::spirit::qi::grammar<Iterator, Skip> {
public:
  grammar() : grammar::base_type(_document) {
    _document =
      *_statement
      ;

    _statement =
      _use
      |
      _namespace
      |
      _interface
      ;

    _use =
      (boost::spirit::lit("use") >> _identifier >> boost::spirit::lit(";")) [ std::cerr << "using package '" << boost::spirit::qi::_1 << "'" << std::endl]
      ;

    _namespace =
      (boost::spirit::lit("ns") >> _identifier >> boost::spirit::lit("{") >> *_statement >> boost::spirit::lit("}")) [std::cerr << "namespace '" << boost::spirit::qi::_1 << "'" << std::endl]
      ;

    _interface =
      (boost::spirit::lit("interface") >> _identifier >> boost::spirit::lit("{") >> boost::spirit::lit("}")) [std::cerr << "interface '" << boost::spirit::qi::_1 << "'" << std::endl]
      ;

    _identifier =
      +boost::spirit::qi::char_("/.a-zA-Z0-9")
      ;

    BOOST_SPIRIT_DEBUG_NODE(_document);
    BOOST_SPIRIT_DEBUG_NODE(_statement);
    BOOST_SPIRIT_DEBUG_NODE(_use);
    BOOST_SPIRIT_DEBUG_NODE(_namespace);
    BOOST_SPIRIT_DEBUG_NODE(_interface);
    BOOST_SPIRIT_DEBUG_NODE(_identifier);
  }
private:
  boost::spirit::qi::rule<Iterator, Skip>                _document;
  boost::spirit::qi::rule<Iterator, Skip>                _use;
  boost::spirit::qi::rule<Iterator, Skip>                _statement;
  boost::spirit::qi::rule<Iterator, Skip>                _namespace;
  boost::spirit::qi::rule<Iterator, Skip>                _interface;
  boost::spirit::qi::rule<Iterator, std::string(), Skip> _identifier;
};
}
}
