// system
#include <iostream>
#include <fstream>
#include <vector>
// boost
#include <boost/spirit/include/support_istream_iterator.hpp>
// rpcxx11
#include <sandbox/silly/grammar.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  for(const auto filename: std::vector<std::string>{argv + 1, argv + argc}) {
#if 0
    std::vector<char> contents;
    std::ifstream in(filename, std::ios::in);
    if(in) {
      in.seekg(0, std::ios::end);
      contents.resize(in.tellg());
      in.seekg(0, std::ios::beg);
      in.read((char *)contents.data(), contents.size());
      in.close();
    }
    auto iter(contents.cbegin());
    auto iend(contents.cend());

    using grammar_iterator_type = std::vector<char>::const_iterator;
#endif
#if 0
    std::ifstream file(filename);
    std::string str((std::istreambuf_iterator<char>(file)),
                     std::istreambuf_iterator<char>());

    auto iter(str.cbegin());
    auto iend(str.cend());

    using grammar_iterator_type = std::string::const_iterator;
#endif
#if 1
    std::cerr << "filename: " << filename << std::endl;
    std::ifstream file(filename);
    file.unsetf(std::ios::skipws);

    // wrap istream into iterator
    boost::spirit::istream_iterator iter(file);
    boost::spirit::istream_iterator iend;
    using grammar_iterator_type = boost::spirit::istream_iterator;
#endif
    sandbox::silly::grammar<grammar_iterator_type, boost::spirit::qi::blank_type> grammar;

    auto parse_status = boost::spirit::qi::phrase_parse(iter, iend, grammar, boost::spirit::qi::blank);
    if(parse_status && iter != iend) {
      std::cerr << "-------------------------" << std::endl
                << "Parsing '" << filename << "' failed" << std::endl
                << "stopped at: \": " << std::string(iter, iend) << "\"" << std::endl
                << "-------------------------" << std::endl;
      throw(std::runtime_error("wavefront parsing failed"));
    }
  }
  return 0;
}
