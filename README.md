rpcxx11
=======

rpc library for c++11

features
--------
* simple serialization
* CDR serialization(CORBA)
* code generation based on c++ source
 
requirements
------------
* for building
    * c++11 compiler
    * cmake
    * python-clang
    * python-mako
    * boost sources
    * nanomsg
* during runtime
    * boost
    * nanomsg

TODO
----
* write documentation
* show examples

example incomplete
------------------
``` cpp
int main(int argc, char **argv) {
  return 0;
}
```
