#!/usr/bin/env python

import argparse
import clang.cindex
import copy
import os
import sys

import mako.template
###############################################################################
def getTypeList(cursor) :
  lst = []
  for c in cursor.get_children() :
    if c.kind == clang.cindex.CursorKind.NAMESPACE_REF or c.kind == clang.cindex.CursorKind.TYPE_REF or c.kind == clang.cindex.CursorKind.TEMPLATE_REF:
      lst.append(str(c.displayname))
  return "::".join(lst)
###############################################################################
def dump_cursor(cursor, indent=''):
  print("%scursor(%s, %s)" % (indent, cursor.spelling, cursor.type))
  for c in cursor.get_children():
    dump_cursor(c, indent+'  ')
###############################################################################
def get_annotations(node):
  return [c.displayname for c in node.get_children()
    if c.kind == clang.cindex.CursorKind.ANNOTATE_ATTR]
###############################################################################
class Param:
  def __init__(self, cursor):
    self.spelling = cursor.type.spelling
    self.type = getTypeList(cursor)
    if self.type == '':
      self.type = self.spelling
###############################################################################
class Function:
  def __init__(self, cursor):
    self.name = cursor.spelling
    self.annotations = get_annotations(cursor)
    self.access = cursor.access_specifier

    self.result = cursor.result_type.spelling

    self.params = []
    for c in cursor.get_children():
      if (c.kind == clang.cindex.CursorKind.PARM_DECL):
        self.params.append(Param(c))

  def hasAnnotation(self):
    for a in self.annotations:
      if a == 'RPCXX11':
        return True
    return False
###############################################################################
class Class:
  def __init__(self, cursor, namespace):
    self.name = cursor.spelling
    self.functions = []
    self.annotations = get_annotations(cursor)
    self.namespace = namespace

    for c in cursor.get_children():
      if (c.kind == clang.cindex.CursorKind.CXX_METHOD and c.access_specifier == clang.cindex.AccessSpecifier.PUBLIC):
        f = Function(c)
        if f.hasAnnotation():
          self.functions.append(f)

  def hasAnnotation(self):
    for f in self.functions:
      if f.hasAnnotation():
        return True
    return False;
###############################################################################
def build_classes(ast_dict, cursor, namespace):
  for c in cursor.get_children():
    if ( (c.kind == clang.cindex.CursorKind.CLASS_DECL or c.kind == clang.cindex.CursorKind.STRUCT_DECL)):
      ast_index=copy.copy(namespace)
      ast_index.append(c.spelling)
      ast_index_name='::'.join(ast_index)
      a_class = Class(c, namespace)
      if a_class.hasAnnotation():
        if not ast_index_name in ast_dict:
          ast_dict[ast_index_name] = a_class
    elif c.kind == clang.cindex.CursorKind.NAMESPACE:
      namespace_copy=copy.deepcopy(namespace)
      namespace_copy.append(c.spelling)
      child_classes = build_classes(ast_dict, c, namespace_copy)
###############################################################################
def gen(c):

  # create output path
  full_scope=['rpcxx11', 'tie'] + c.namespace
  tie_path = '%s' % ('/'.join(full_scope))
  tie_filename = '%s/%s.h' % (tie_path, c.name)
  try:
    os.makedirs(tie_path)
  except OSError as e:
    pass

  # generate
  tie_dict = {}
  tie_dict["namespace"]=full_scope
  tie_dict["object"]=c
  tie_template = mako.template.Template(filename='%s/tie.mako' % os.path.abspath(os.path.dirname(sys.argv[0])))
  tie_render = tie_template.render(**tie_dict)

  # write file
  with open(tie_filename, 'w') as tie_file:
    tie_file.write(tie_render)
###############################################################################
def dump(c):
  print('class: %s(ns=%s)' % (c.name, '::'.join(c.namespace)))
  # print('  annotations: %s' % (c.annotations))
  for f in c.functions:
    print('  function: %s' % (f.name))
    # print('    annotations: %s' % (f.annotations))
    # print('    access: %s' % (f.access))
    print('    result: %s' % (f.result))
    for p in f.params:
      print('    param: %s' % (p.spelling))
      print('           %s' % (p.type))
###############################################################################
if __name__=='__main__':
  parser  = argparse.ArgumentParser()
  parser.add_argument('--gendir', help='dir to put files in', action='store_true')
  parser.add_argument('--include', dest='include', action='append')

  (args, unknown_args) = parser.parse_known_args()

  ad = {}

  options=['-x', 'c++', '-std=c++11']
  if args.include:
    for inc in args.include:
        options.append('--include')
        options.append(inc)

  # print('options:', options)

  # parse
  index = clang.cindex.Index.create()
  for f in unknown_args:
    print('f: %s' % (f))
    tu = index.parse(f, options)
    build_classes(ad, tu.cursor, [])

  # gen
  for i,c in ad.iteritems():
    dump(c)
    gen(c)
