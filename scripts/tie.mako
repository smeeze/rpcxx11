#pragma once

//
// auto generated file. Do not edit.
//

// system
#include <functional>
#include <map>

// start namespaces
% for ns in namespace:
namespace ${ns} {
% endfor

class ${object.name} {
public:
  ///////////////////////////////////////////////////////////////////////////////
  // proxy                                                                     //
  ///////////////////////////////////////////////////////////////////////////////
  template<typename Session>
  class proxy {
  public:
    //
    // constructor
    //
    proxy(const Session & p, const typename Session::servant_id_type & si) : _session(p), _servant_id(si) { }

    % for f in object.functions:
    //
    // registered function ${loop.index}: ${f.name}
    //
    ${f.result} ${f.name}(
      % for p in f.params:
        % if loop.index > 0:
        ,
        % endif
        ${p.spelling} arg${loop.index}
      % endfor
    ) {
      // start transaction
      auto transaction(_session.build_proxy_transaction(_servant_id, Session::gen_function_id(${loop.index})));

      // std::cerr << __PRETTY_FUNCTION__ << std::endl;
      // std::cerr << "  in: " << **transaction.marshal << std::endl;
      // std::cerr << "  out: " << **transaction.unmarshal << std::endl;

      // marshal arguments
      % for p in f.params:
      transaction.marshal & arg${loop.index};
      // std::cerr << "marshalling ${p.type}(" << arg${loop.index} << ")" << std::endl;
      % endfor

      // commit transaction
      transaction.commit();

      % if f.result != 'void':
      // return data
      ${f.result} return_value;
      transaction.unmarshal & return_value;
      return return_value;
      % endif
    }
    % endfor
  private:
    Session                           _session;
    typename Session::servant_id_type _servant_id;
  };
  ///////////////////////////////////////////////////////////////////////////////
  // servant                                                                   //
  ///////////////////////////////////////////////////////////////////////////////
  template<typename Session, typename T>
  class servant {
  public:
    //
    // constructor
    //
    servant(const T & t) : _t(t) {
      % for f in object.functions:
      //
      // registered function ${loop.index}
      // ${f.name}
      //
      _function_map.insert(
        std::make_pair<typename Session::function_id_type, _function_type>(
          Session::gen_function_id(${loop.index}),
          [this](typename Session::servant_transaction & transaction) {

            // unmarshal arguments
            % for p in f.params:
            ${p.type} arg${loop.index};
            transaction.unmarshal & arg${loop.index};
            // std::cerr << "unmarshalling ${p.type}(" << arg${loop.index} << ")" << std::endl;
            % endfor

            // call
            % if f.result != 'void':
            auto result =
            % endif
            _t->${f.name}(
              % for p in f.params:
              % if loop.index > 0:
              ,
              % endif
              arg${loop.index}
              % endfor
            );

            % if f.result != 'void':
            // marshal result
            transaction.marshal & result;
            % endif
          }
        )
      );
      % endfor
    }

    //
    // handle 
    //
    void handle(const typename Session::function_id_type & function_id, typename Session::servant_transaction & t) {
      _function_map[function_id](t);
    }
  private:
    typedef std::function<void(typename Session::servant_transaction &)> _function_type;
    std::map<typename Session::function_id_type, _function_type>         _function_map;
    T                                                                    _t;
  };

};

// end namespaces
% for ns in namespace:
} // ${ns}
% endfor
