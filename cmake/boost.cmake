cmake_minimum_required(VERSION 3.4 FATAL_ERROR)

###############################################################################
if(DO_BOOST_REGEX)
  add_library(BoostRegexObjects OBJECT
    ${BOOST_SOURCES}/libs/regex/src/cpp_regex_traits.cpp
    ${BOOST_SOURCES}/libs/regex/src/cregex.cpp
    ${BOOST_SOURCES}/libs/regex/src/c_regex_traits.cpp
    ${BOOST_SOURCES}/libs/regex/src/fileiter.cpp
    ${BOOST_SOURCES}/libs/regex/src/icu.cpp
    ${BOOST_SOURCES}/libs/regex/src/instances.cpp
    ${BOOST_SOURCES}/libs/regex/src/internals.hpp
    ${BOOST_SOURCES}/libs/regex/src/posix_api.cpp
    ${BOOST_SOURCES}/libs/regex/src/regex.cpp
    ${BOOST_SOURCES}/libs/regex/src/regex_debug.cpp
    ${BOOST_SOURCES}/libs/regex/src/regex_raw_buffer.cpp
    ${BOOST_SOURCES}/libs/regex/src/regex_traits_defaults.cpp
    ${BOOST_SOURCES}/libs/regex/src/static_mutex.cpp
    ${BOOST_SOURCES}/libs/regex/src/usinstances.cpp
    ${BOOST_SOURCES}/libs/regex/src/w32_regex_traits.cpp
    ${BOOST_SOURCES}/libs/regex/src/wc_regex_traits.cpp
    ${BOOST_SOURCES}/libs/regex/src/wide_posix_api.cpp
    ${BOOST_SOURCES}/libs/regex/src/winstances.cpp
  )
  add_library(BoostRegexStatic          STATIC $<TARGET_OBJECTS:BoostRegexObjects>)
  add_library(BoostRegexShared          SHARED $<TARGET_OBJECTS:BoostRegexObjects>)
  target_include_directories(BoostRegexObjects          SYSTEM PRIVATE ${BOOST_SOURCES})
  target_compile_options(BoostRegexObjects              PRIVATE ${ARCH64_FLAGS})
  target_compile_definitions(BoostRegexObjects          PRIVATE BOOST_ALL_NO_LIB BOOST_ALL_DYN_LINK)
endif()
###############################################################################
if(DO_BOOST_SYSTEM)
  add_library(BoostSystemObjects OBJECT
    ${BOOST_SOURCES}/libs/system/src/error_code.cpp
  )
  add_library(BoostSystemStatic         STATIC $<TARGET_OBJECTS:BoostSystemObjects>)
  add_library(BoostSystemShared         SHARED $<TARGET_OBJECTS:BoostSystemObjects>)
  target_include_directories(BoostSystemObjects         SYSTEM PRIVATE ${BOOST_SOURCES})
  target_compile_options(BoostSystemObjects             PRIVATE ${ARCH64_FLAGS})
  target_compile_definitions(BoostSystemObjects         PRIVATE BOOST_ALL_NO_LIB BOOST_ALL_DYN_LINK)
endif()
###############################################################################
if(DO_BOOST_FILESYSTEM)
  add_library(BoostFileSystemObjects OBJECT
    ${BOOST_SOURCES}/libs/filesystem/src/codecvt_error_category.cpp
    ${BOOST_SOURCES}/libs/filesystem/src/operations.cpp
    ${BOOST_SOURCES}/libs/filesystem/src/path.cpp
    ${BOOST_SOURCES}/libs/filesystem/src/path_traits.cpp
    ${BOOST_SOURCES}/libs/filesystem/src/portability.cpp
    ${BOOST_SOURCES}/libs/filesystem/src/unique_path.cpp
    ${BOOST_SOURCES}/libs/filesystem/src/utf8_codecvt_facet.cpp
    ${BOOST_SOURCES}/libs/filesystem/src/windows_file_codecvt.cpp
  )
  add_library(BoostFileSystemStatic     STATIC $<TARGET_OBJECTS:BoostFileSystemObjects>)
  add_library(BoostFileSystemShared     SHARED $<TARGET_OBJECTS:BoostFileSystemObjects>)
  target_include_directories(BoostFileSystemObjects     SYSTEM PRIVATE ${BOOST_SOURCES})
  target_compile_options(BoostFileSystemObjects         PRIVATE ${ARCH64_FLAGS})
  target_compile_definitions(BoostFileSystemObjects     PRIVATE BOOST_ALL_NO_LIB BOOST_ALL_DYN_LINK)
  target_link_libraries(BoostFileSystemShared BoostSystemShared)
endif()
###############################################################################
if(DO_BOOST_PROGRAM_OPTIONS)
  add_library(BoostProgramOptionsObjects OBJECT
    ${BOOST_SOURCES}/libs/program_options/src/cmdline.cpp
    ${BOOST_SOURCES}/libs/program_options/src/config_file.cpp
    ${BOOST_SOURCES}/libs/program_options/src/convert.cpp
    ${BOOST_SOURCES}/libs/program_options/src/options_description.cpp
    ${BOOST_SOURCES}/libs/program_options/src/parsers.cpp
    ${BOOST_SOURCES}/libs/program_options/src/positional_options.cpp
    ${BOOST_SOURCES}/libs/program_options/src/split.cpp
    ${BOOST_SOURCES}/libs/program_options/src/utf8_codecvt_facet.cpp
    ${BOOST_SOURCES}/libs/program_options/src/value_semantic.cpp
    ${BOOST_SOURCES}/libs/program_options/src/variables_map.cpp
    ${BOOST_SOURCES}/libs/program_options/src/winmain.cpp
  )
  add_library(BoostProgramOptionsStatic STATIC $<TARGET_OBJECTS:BoostProgramOptionsObjects>)
  add_library(BoostProgramOptionsShared SHARED $<TARGET_OBJECTS:BoostProgramOptionsObjects>)
  target_include_directories(BoostProgramOptionsObjects SYSTEM PRIVATE ${BOOST_SOURCES})
  target_compile_options(BoostProgramOptionsObjects PRIVATE ${ARCH64_FLAGS})
  target_compile_definitions(BoostProgramOptionsObjects PRIVATE BOOST_ALL_NO_LIB BOOST_ALL_DYN_LINK)
  target_link_libraries(BoostProgramOptionsShared BoostSystemShared)
endif()
###############################################################################
if(DO_BOOST_CHRONO)
  add_library(BoostChronoObjects OBJECT
    ${BOOST_SOURCES}/libs/chrono/src/chrono.cpp
    ${BOOST_SOURCES}/libs/chrono/src/process_cpu_clocks.cpp
    ${BOOST_SOURCES}/libs/chrono/src/thread_clock.cpp
  )
  add_library(BoostChronoStatic         STATIC $<TARGET_OBJECTS:BoostChronoObjects>)
  add_library(BoostChronoShared         SHARED $<TARGET_OBJECTS:BoostChronoObjects>)
  target_include_directories(BoostChronoObjects         SYSTEM PRIVATE ${BOOST_SOURCES})
  target_compile_options(BoostChronoObjects             PRIVATE ${ARCH64_FLAGS})
  target_compile_definitions(BoostChronoObjects         PRIVATE BOOST_ALL_NO_LIB BOOST_ALL_DYN_LINK)
  target_link_libraries(BoostChronoShared BoostSystemShared)
endif()
###############################################################################
if(DO_BOOST_DATE_TIME)
  add_library(BoostDateTimeObjects OBJECT
    ${BOOST_SOURCES}/libs/date_time/src/gregorian/date_generators.cpp
    ${BOOST_SOURCES}/libs/date_time/src/gregorian/greg_month.cpp
    ${BOOST_SOURCES}/libs/date_time/src/gregorian/gregorian_types.cpp
    ${BOOST_SOURCES}/libs/date_time/src/gregorian/greg_weekday.cpp
    ${BOOST_SOURCES}/libs/date_time/src/posix_time/posix_time_types.cpp
  )
  add_library(BoostDateTimeStatic       STATIC $<TARGET_OBJECTS:BoostDateTimeObjects>)
  add_library(BoostDateTimeShared       SHARED $<TARGET_OBJECTS:BoostDateTimeObjects>)
  target_include_directories(BoostDateTimeObjects       SYSTEM PRIVATE ${BOOST_SOURCES})
  target_compile_options(BoostDateTimeObjects           PRIVATE ${ARCH64_FLAGS})
  target_compile_definitions(BoostDateTimeObjects       PRIVATE BOOST_ALL_NO_LIB BOOST_ALL_DYN_LINK)
endif()
###############################################################################
if(DO_BOOST_PYTHON)
  find_package(PythonLibs)
  if(PYTHONLIBS_FOUND)
    add_library(BoostPythonObjects OBJECT
      ${BOOST_SOURCES}/libs/python/src/str.cpp
      ${BOOST_SOURCES}/libs/python/src/tuple.cpp
      ${BOOST_SOURCES}/libs/python/src/errors.cpp
      ${BOOST_SOURCES}/libs/python/src/list.cpp
      ${BOOST_SOURCES}/libs/python/src/dict.cpp
      ${BOOST_SOURCES}/libs/python/src/numeric.cpp
      ${BOOST_SOURCES}/libs/python/src/object_operators.cpp
      ${BOOST_SOURCES}/libs/python/src/long.cpp
      ${BOOST_SOURCES}/libs/python/src/exec.cpp
      ${BOOST_SOURCES}/libs/python/src/slice.cpp
      ${BOOST_SOURCES}/libs/python/src/converter/arg_to_python_base.cpp
      ${BOOST_SOURCES}/libs/python/src/converter/builtin_converters.cpp
      ${BOOST_SOURCES}/libs/python/src/converter/registry.cpp
      ${BOOST_SOURCES}/libs/python/src/converter/from_python.cpp
      ${BOOST_SOURCES}/libs/python/src/converter/type_id.cpp
      ${BOOST_SOURCES}/libs/python/src/module.cpp
      ${BOOST_SOURCES}/libs/python/src/wrapper.cpp
      ${BOOST_SOURCES}/libs/python/src/import.cpp
      ${BOOST_SOURCES}/libs/python/src/object_protocol.cpp
      ${BOOST_SOURCES}/libs/python/src/object/inheritance.cpp
      ${BOOST_SOURCES}/libs/python/src/object/function.cpp
      ${BOOST_SOURCES}/libs/python/src/object/pickle_support.cpp
      ${BOOST_SOURCES}/libs/python/src/object/function_doc_signature.cpp
      ${BOOST_SOURCES}/libs/python/src/object/life_support.cpp
      ${BOOST_SOURCES}/libs/python/src/object/stl_iterator.cpp
      ${BOOST_SOURCES}/libs/python/src/object/iterator.cpp
      ${BOOST_SOURCES}/libs/python/src/object/class.cpp
      ${BOOST_SOURCES}/libs/python/src/object/enum.cpp
    )
    add_library(BoostPythonStatic         STATIC $<TARGET_OBJECTS:BoostPythonObjects>)
    add_library(BoostPythonShared         SHARED $<TARGET_OBJECTS:BoostPythonObjects>)
    target_include_directories(BoostPythonObjects         SYSTEM PRIVATE ${BOOST_SOURCES} PRIVATE ${PYTHON_INCLUDE_DIRS})
    target_compile_options(BoostPythonObjects             PRIVATE ${ARCH64_FLAGS})
    target_compile_definitions(BoostPythonObjects         PRIVATE BOOST_ALL_NO_LIB BOOST_PYTHON_SOURCE)
    target_link_libraries(BoostPythonShared ${PYTHON_LIBRARIES})
  endif()
endif()
