// system
#include <iostream>
// rpcxx11
#include <rpcxx11/delegate/data_view.h>
#include <rpcxx11/delegate/debug.h>
#include <rpcxx11/modules/binary/serialize.h>
///////////////////////////////////////////////////////////////////////////////
#define MARKER { std::cerr << "marker: " << __FILE__ << "(" << __LINE__ << ")" << std::endl; }
///////////////////////////////////////////////////////////////////////////////
namespace {
  template<typename M, typename U>
  void
  run_test(const M & m, const U & u) {
    std::cerr << "-------------------------------------------------------------------------------" << std::endl;

    // copy

    std::cerr << "-- marshalling" << std::endl;
    auto        marshal(m);
    std::string in("test string");
    marshal & in;

    std::cerr << "-- unmarshalling" << std::endl;
    auto        unmarshal(u);
    std::string out;
    unmarshal & out;

    std::cerr << "-- in: " << in << std::endl;
    std::cerr << "-- out: " << out << std::endl;
    std::cerr << "-- *marshal: " << *marshal << std::endl;
    std::cerr << "-- *unmarshal: " << *unmarshal << std::endl;

    assert(in == out);
  }
}
///////////////////////////////////////////////////////////////////////////////
int
main(int, char **) {
  std::vector<char> buffer(1024);

  typedef rpcxx11::modules::binary::serialize     serialize_type;
  typedef rpcxx11::delegate::data_view<char>      delegate_type;
  {
    serialize_type::marshal_type<delegate_type>   marshal(buffer);
    serialize_type::unmarshal_type<delegate_type> unmarshal(buffer);

    run_test(marshal, unmarshal);
  }

  typedef rpcxx11::delegate::debug<delegate_type> delegate_debug_type;
  {
    delegate_type                                       marshal_delegate(buffer);
    serialize_type::marshal_type<delegate_debug_type>   marshal(marshal_delegate);
    delegate_type                                       unmarshal_delegate(buffer);
    serialize_type::unmarshal_type<delegate_debug_type> unmarshal(unmarshal_delegate);

    run_test(marshal, unmarshal);
  }

  return 0;
}
