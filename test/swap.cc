// system
#include <cassert>
#include <cstdint>
#include <functional>
#include <iostream>
#include <chrono>
// boost
#include <boost/endian/conversion.hpp>
// rpcxx11
#include <rpcxx11/utils/swap.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  const uint32_t start_value = 285;
  ///////////////////////////////////////////////////////////////////////////////
  void
  test_boost() {
    uint32_t n(start_value);

    boost::endian::native_to_big_inplace(n);
    assert(n == 486604800);
    // std::cerr << "n: " << n << std::endl;

    boost::endian::big_to_native_inplace(n);
    assert(n == start_value);
    // std::cerr << "n: " << n << std::endl;
  }
  ///////////////////////////////////////////////////////////////////////////////
  void
  test_rpcxx11() {
    uint32_t n(start_value);

    rpcxx11::utils::swap::in_place(n);
    assert(n == 486604800);
    // std::cerr << "n: " << n << std::endl;

    rpcxx11::utils::swap::in_place(n);
    assert(n == start_value);
    // std::cerr << "n: " << n << std::endl;
  }
  ///////////////////////////////////////////////////////////////////////////////
  void
  time_test(const std::string & msg, const std::function<void()> & f) {
    uint32_t n(0);
    auto start(std::chrono::steady_clock::now());
    auto stop(start+std::chrono::seconds(3));
    while(std::chrono::steady_clock::now() < stop) {
        f();
        ++n;
      }
    std::cerr << msg << " done in " << n << " loops" << std::endl;
  }
}
///////////////////////////////////////////////////////////////////////////////
int
main(int, char **) {
  std::cerr << BOOST_BYTE_ORDER << std::endl;
  time_test("rpcxx11", test_rpcxx11);
  time_test("boost", test_boost);

  return 0;
}
