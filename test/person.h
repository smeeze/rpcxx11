#pragma once

// system
#include <iostream>

namespace test {
class person {
public:
  person() { }
  person(const std::string &f, const std::string &l) : _first_name(f), _last_name(l) { }

  template<typename Archive>
  void serialize(Archive & ar) {
    ar & _first_name;
    ar & _last_name;
  }

  bool operator==(const person & p) const { return getFirstName() == p.getFirstName() && getLastName() == p.getLastName(); }
  bool operator!=(const person & p) const { return getFirstName() != p.getFirstName() || getLastName() != p.getLastName(); }

  const std::string & getFirstName() const { return _first_name; }
  const std::string & getLastName() const { return _last_name; }
private:
  std::string _first_name;
  std::string _last_name;
};
}

inline
std::ostream& operator<<(std::ostream &out, const test::person &p) {
  out << "test::person(" << p.getFirstName() << ", " << p.getLastName() << ")";
  return out;
}   
