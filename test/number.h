#pragma once

// system
#include <cassert>
#include <iostream>

#ifdef __CODE_GENERATOR__
#define TAG_KWYJIBO __attribute__((annotate("kwyjibo")))
#else
#define TAG_KWYJIBO
#endif

namespace test {
class writer {
public:
  writer(double & storage) : _storage(storage) {
    std::cerr << __PRETTY_FUNCTION__ << "()" << std::endl;
  }
  void operator()(const void *ptr, uint32_t size) {
    assert(size == sizeof(double));
    std::cerr << __PRETTY_FUNCTION__ << "(*, " << size << ")" << std::endl;
    _storage = * static_cast<const double *>(ptr);
  }
  double & _storage;
};

class reader {
public:
  reader(const double & storage) : _storage(storage) {
    std::cerr << __PRETTY_FUNCTION__ << "()" << std::endl;
  }
  void operator()(void *ptr, uint32_t size) {
    assert(size == sizeof(double));
    std::cerr << __PRETTY_FUNCTION__ << "(*, " << size << ")" << std::endl;
    * static_cast<double *>(ptr) = _storage;
  }
  const double & _storage;
};

class number {
public:
  number() : number(0) { }
  number(const double & t) : _t(t) {
  };

  const double & operator*() const {
    return _t;
  }

  void      set(const double & t) TAG_KWYJIBO { _t = t; }
  const double & set() const                  { return _t; }

  bool operator==(const number & o) const {
    return _t == o._t;
  }
  bool operator!=(const number & o) const {
    return _t != o._t;
  }
#if 1
  template<typename Archive>
  void marshal(Archive & ar) {
    ar & _t;
    std::cerr << __PRETTY_FUNCTION__ << "(" << _t << ")" << std::endl;
  }

  template<typename Archive>
  void unmarshal(Archive & ar) {
    ar & _t;
    std::cerr << __PRETTY_FUNCTION__ << "(" << _t << ")" << std::endl;
  }
#else
  template<typename Archive>
  void serialize(Archive & ar) {
    ar & _t;
    std::cerr << __PRETTY_FUNCTION__ << "(" << _t << ")" << std::endl;
  }
#endif
private:
  void zero() { _t = 0; }
private:
  double _t;
};
}

inline
std::ostream &
operator<<(std::ostream & out, const test::number & n) {
  out << *n;
  return out;
}
