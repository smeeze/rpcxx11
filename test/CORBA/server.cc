// system
#include <chrono>
#include <memory>
#include <string>
#include <thread>
// boost
#include <boost/format.hpp>
// rpcxx11
#include <rpcxx11/wire/handler.h>
#include <rpcxx11/modules/asio/server.h>
#include <rpcxx11/modules/CORBA/request.h>
#include <rpcxx11/modules/CORBA/serialize.h>
#include <rpcxx11/utils/file.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  class handler : public rpcxx11::wire::handler {
    uint32_t _dump_id;
  public:
    rpcxx11::delegate::data_view<char> handle(const rpcxx11::delegate::data_view<char> & in, const rpcxx11::delegate::data_view<char> & out) override {
      auto result(out);
      std::cerr << __PRETTY_FUNCTION__ << std::endl;
      std::cerr << "  in: " << in << std::endl;
      std::cerr << "  out: " << out << std::endl;
      std::cerr << "  result: " << result << std::endl;

      rpcxx11::modules::CORBA::unmarshal<rpcxx11::delegate::data_view<char>> unmarshal(in);
      rpcxx11::modules::CORBA::marshal<rpcxx11::delegate::data_view<char>>   marshal(out);
#ifdef DO_DUMP_REQUEST
      {
        rpcxx11::utils::file dump_file(boost::str(boost::format("connection-request-%i.dump") % _dump_id++), rpcxx11::utils::file::mode::write);
        dump_file.write(in.begin, in.get_available());
      }
#endif
      rpcxx11::modules::CORBA::request r;
      unmarshal & r;
      std::cerr << r;

#ifdef DO_DUMP_RESULT
      {
        rpcxx11::utils::file dump_file(boost::str(boost::format("connection-result-%i.dump") % _dump_id++), rpcxx11::utils::file::mode::write);
        dump_file.write(result.begin, result.get_available());
      }
#endif

      return result;
    }
  };
}
///////////////////////////////////////////////////////////////////////////////
int
main(int, char **) {
  auto server(std::make_shared<rpcxx11::modules::asio::server>("0.0.0.0", 1313, std::make_shared<handler>()));

  while(true) {
      std::this_thread::sleep_for(std::chrono::seconds(1));
    }

  return 0;
}
