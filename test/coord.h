#pragma once

// system
#include <iostream>

namespace test {
template<typename T>
class coord {
public:
  coord() : coord(0, 0, 0) { }
  coord(T x, T y, T z) : _x(x), _y(y), _z(z) { }

  template<typename Archive>
  void serialize(Archive & ar) {
    ar & _x;
    ar & _y;
    ar & _z;
  }

  bool operator==(const coord & p) const { return x() == p.x() && y() == p.y() && z() == p.z(); }
  bool operator!=(const coord & p) const { return x() != p.x() || y() != p.y() || z() != p.z(); }

  const T & x() const { return _x; }
  const T & y() const { return _y; }
  const T & z() const { return _z; }
private:
  T _x;
  T _y;
  T _z;
};
}

template<typename T>
inline
std::ostream &
operator<<(std::ostream &out, const test::coord<T> &c) {
  out << "test::coord(" << c.x() << ", " << c.y() << ", " << c.z() << ")";
  return out;
}

