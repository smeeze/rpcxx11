#pragma once

// system
#include <iostream>
#include <string>
// boost
#include <boost/format.hpp>

namespace base {
class remote {
public:
  virtual ~remote() { } 

  bool _is_a(const std::string & arg) __attribute__((annotate("RPCXX11"))) {
    std::cerr << __PRETTY_FUNCTION__ << "(" << arg << ")" << std::endl;
    return true;
  }
};
}

namespace test {
namespace rpc {
class remote : public base::remote {
public:
  class person {
  public:
    std::string name;
    uint32_t    age;

    template<typename Archive>
    void serialize(Archive & ar) {
      ar & name;
      ar & age;
    }
  };

  ::test::rpc::remote::person who(const std::string &name) __attribute__((annotate("RPCXX11"))) {
    return person{name, 13};
  }

  int sum(int a, int b) __attribute__((annotate("RPCXX11"))) {
    return a+b;
  }

  std::string echo(const std::string &msg) __attribute__((annotate("RPCXX11"))) {
    return boost::str(
      boost::format(
        "you wrote: '%s'"
      ) % msg
    );
  }
};
}
}

inline
std::ostream &
operator<<(std::ostream & out, const test::rpc::remote::person & p) {
  out << "person(name=" << p.name << ", age=" << p.age << ")";
  return out;
}
