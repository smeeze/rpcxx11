// system
#include <chrono>
#include <iostream>
#include <thread>
// boost
#include <boost/program_options.hpp>
// rpcxx11
#include <rpcxx11/factory.h>
// self
#include <test/rpc/handler.h>
#include <test/rpc/registry.h>
#include <test/rpc/defines.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  std::string                                           endpoint;
  test::rpc::registry<test::rpc::defines::session_type> registry;
  boost::program_options::options_description           desc("options");
  desc.add_options()
    ("help",     "produce help message")
    ("endpoint", boost::program_options::value<std::string>(&endpoint)->default_value("asio://0.0.0.0:1313"), "endpoint to connect to")
  ;

  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).run(), vm);

  if (vm.count("help")) {
    std::cout << desc;
    return 0;
  }

  boost::program_options::notify(vm);

  auto handler(std::make_shared<test::rpc::handler<test::rpc::registry<test::rpc::defines::session_type>>>(registry));
  auto server(rpcxx11::factory::wire::build_server(endpoint, handler));

  while(true) {
    std::this_thread::sleep_for(std::chrono::seconds(1));
  }

  return 0;
}
