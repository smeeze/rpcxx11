#pragma once

// system
#include <string>
// rpcxx11
#include <rpcxx11/wire/handler.h>

namespace test {
namespace rpc {
template<typename Registry>
class handler : public rpcxx11::wire::handler {
public:
  handler(const Registry & registry) : _registry(registry) { }

  rpcxx11::delegate::data_view<char> handle(const rpcxx11::delegate::data_view<char> & in, const rpcxx11::delegate::data_view<char> & out) override {
    return _registry.handle(in, out);
  }
private:
  Registry _registry;
};
}
}
