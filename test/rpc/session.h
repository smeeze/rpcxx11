#pragma once

// system
#include <iostream>
#include <string>
// boost
#include <boost/format.hpp>
// rpcxx11
#include <rpcxx11/delegate/data_view.h>
#include <rpcxx11/wire/handler.h>
#include <rpcxx11/modules/binary/serialize.h>

namespace test {
namespace rpc {
class session {
public:
  typedef uint32_t                                             servant_id_type;
  typedef std::string                                          function_id_type;
  typedef rpcxx11::delegate::data_view<char>                   delegate_type;
  typedef rpcxx11::modules::binary::serialize                  serialize_type;
  typedef rpcxx11::wire::handler::pointer                      client_type;
  typedef serialize_type::marshal_type<delegate_type>          marshal_type;
  typedef serialize_type::unmarshal_type<delegate_type>        unmarshal_type;

  session(client_type client) : _client(client), _marshal_buffer(1024), _unmarshal_buffer(1024) { }

  template<typename T>
  static
  function_id_type gen_function_id(const T & t);

  static
  function_id_type gen_function_id(const int & i) { return boost::str(boost::format("func%i") % i); }

  //
  // header
  //
  class header_type {
  public:
    
    servant_id_type  servant_id;
    function_id_type function_id;

    template<typename T>
    void operator&(T & t) {
      t & servant_id;
      t & function_id;
    }
  };

  //
  // proxy transaction
  //
  class proxy_transaction {
  public:
    proxy_transaction(const client_type & client, const delegate_type & marshal_delegate, const delegate_type & unmarshal_delegate) : marshal(marshal_delegate), unmarshal(unmarshal_delegate), _client(client) {
    }

    marshal_type   marshal;
    unmarshal_type unmarshal;

    void commit() {
      auto in(*marshal);
      auto out(*unmarshal);

      _client->handle(in, out);
    }
  private:
    client_type _client;
  };

  //
  // build proxy transaction
  //
  proxy_transaction build_proxy_transaction(servant_id_type servant_id, function_id_type function_id) {
    rpcxx11::delegate::data_view<char> marshal_data_view(_marshal_buffer);
    rpcxx11::delegate::data_view<char> unmarshal_data_view(_unmarshal_buffer);

    delegate_type                      marshal_delegate(marshal_data_view);
    delegate_type                      unmarshal_delegate(unmarshal_data_view);

    marshal_type                       marshal(marshal_delegate);
    
    header_type header{servant_id, function_id};
    header & marshal;

    return proxy_transaction(_client, *marshal, unmarshal_delegate);
  }

  //
  // servant transaction
  //
  class servant_transaction {
  public:
    header_type    header;
    unmarshal_type unmarshal;
    marshal_type   marshal;
  };

  //
  // build servant transaction
  //
  static servant_transaction build_servant_transaction(const delegate_type & unmarshal_delegate, const delegate_type & marshal_delegate) {

    unmarshal_type unmarshal(unmarshal_delegate);
    header_type header;
    header & unmarshal;

    return servant_transaction{header, *unmarshal, marshal_type(marshal_delegate)};
  }

  client_type       _client;
  std::vector<char> _marshal_buffer;
  std::vector<char> _unmarshal_buffer;
};
}
}

inline
std::ostream & operator<<(std::ostream & out, const test::rpc::session::header_type & h) {
  out << "test::rpc::session::header_type("
      << h.servant_id
      << ", "
      << h.function_id
      << ")"
      ;
  return out;
}
