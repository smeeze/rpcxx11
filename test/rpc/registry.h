#pragma once

// system
#include <map>
// rpcxx11
#include <rpcxx11/delegate/data_view.h>
// self
#include <test/rpc/remote.h>
#include <test/rpc/session.h>
#include <rpcxx11/tie/test/rpc/remote.h>

namespace test {
namespace rpc {
template<typename Session>
class registry {
public:
  typedef std::shared_ptr<rpcxx11::tie::test::rpc::remote::servant<Session, std::shared_ptr<test::rpc::remote>>> servant_type;

  rpcxx11::delegate::data_view<char> handle(const rpcxx11::delegate::data_view<char> & in, const rpcxx11::delegate::data_view<char> & out) {
    auto result(out);

    // types
    typename Session::delegate_type  delegate_unmarshal(in);
    typename Session::delegate_type  delegate_marshal(out);

    auto st(Session::build_servant_transaction(delegate_unmarshal, delegate_marshal));

    auto servant(activate_servant(st.header.servant_id));
    auto offset_pre(st.marshal.get_offset());
    servant->handle(st.header.function_id, st);
    auto offset_post(st.marshal.get_offset());
    deactivate_servant(st.header.servant_id);

    return result += (offset_post - offset_pre);
  }

  servant_type activate_servant(typename Session::servant_id_type servant_id) {
    if(_servant_map.find(servant_id) == _servant_map.end()) {
      _servant_map[servant_id] = std::make_shared<rpcxx11::tie::test::rpc::remote::servant<Session, std::shared_ptr<test::rpc::remote>>>(std::make_shared<test::rpc::remote>());
    }
    return _servant_map[servant_id];
  }
  void deactivate_servant(typename Session::servant_id_type /* servant_id */) {
  }
private:
  std::map<typename Session::servant_id_type, servant_type> _servant_map;
};
}
}
