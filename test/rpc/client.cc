// system
#include <cassert>
#include <chrono>
#include <iostream>
#include <random>
// boost
#include <boost/program_options.hpp>
// rpcxx11
#include <rpcxx11/factory.h>
// self
#include <test/rpc/remote.h>
#include <test/rpc/handler.h>
#include <test/rpc/defines.h>
#include <rpcxx11/tie/test/rpc/remote.h>
///////////////////////////////////////////////////////////////////////////////
int
main(int argc, char ** argv) {
  std::string                                 message;
  std::string                                 endpoint;
  boost::program_options::options_description desc("options");
  desc.add_options()
    ("help",     "produce help message")
    ("endpoint", boost::program_options::value<std::string>(&endpoint)->default_value("asio://localhost:1313"), "endpoint to connect to")
    ("message",  boost::program_options::value<std::string>(&message)->default_value("hello"), "message to send")
  ;

  boost::program_options::variables_map vm;
  boost::program_options::store(boost::program_options::command_line_parser(argc, argv).options(desc).run(), vm);

  if (vm.count("help")) {
    std::cout << desc;
    return 0;
  }

  boost::program_options::notify(vm);

  rpcxx11::tie::test::rpc::remote::proxy<test::rpc::defines::session_type> remote_proxy(
    test::rpc::defines::session_type(rpcxx11::factory::wire::build_handler(endpoint)),
    25
  );


  std::random_device              rd;
  std::mt19937                    gen(rd());
  std::uniform_int_distribution<> dis(1, 10);

  uint32_t n(0);
  auto start(std::chrono::steady_clock::now());
  while(std::chrono::steady_clock::now() < (start+std::chrono::seconds(3))) {
    auto a(dis(gen));
    auto b(dis(gen));
    auto r(remote_proxy.sum(a, b));

    if(n==0) {
      std::cerr << "remote_proxy.sum(" << a << ", " << b << ")= " << r << std::endl;
    }

    assert(a+b==r);
    ++n;
  }
  auto stop(std::chrono::steady_clock::now());
  std::cerr << "calls per second: " << 1000 * n / std::chrono::duration_cast<std::chrono::milliseconds>(stop-start).count() << std::endl;

  std::cerr << "echo(" << message << "): " << remote_proxy.echo(message) << std::endl;

  std::string who("Robert");
  std::cerr << "who(" << who << "): " << remote_proxy.who(who) << std::endl;

  return 0;
}
