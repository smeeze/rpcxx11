#pragma once

// rpcxx11
#include <rpcxx11/modules/CORBA/session.h>
#include <rpcxx11/modules/debug/session.h>
// self
#include <test/rpc/session.h>
///////////////////////////////////////////////////////////////////////////////
namespace test {
namespace rpc {
namespace defines {
  // typedef rpcxx11::modules::CORBA::session session_type;
  // typedef rpcxx11::modules::debug::session session_type;
  typedef test::rpc::session session_type;
};
}
}
