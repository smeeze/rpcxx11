#include <rpcxx11/utils/uri.h>

int
main(int, char **) {
  rpcxx11::utils::uri uri("tcp://localhost:1313");
  uri.dump();

  return 0;
}
