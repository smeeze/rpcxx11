// system
#include <cstring>
#include <iostream>
#include <vector>
// rpcxx11
#include <rpcxx11/delegate/data_view.h>
#include <rpcxx11/exceptions/invalid.h>
#include <rpcxx11/modules/binary/serialize.h>
// test
#include <test/coord.h>
#include <test/number.h>
#include <test/person.h>
///////////////////////////////////////////////////////////////////////////////
#define MARKER(a) { std::cerr << "--- marker(" << a << ") ---" << std::endl; }
///////////////////////////////////////////////////////////////////////////////
template<typename T>
bool
operator==(const std::vector<T> &a, const std::vector<T> &b) {
  // size must be equal
  if(a.size() != b.size()) { return false; }

  auto bi(b.cbegin());
  for(const auto & ai: a) {
      if(*bi != ai) {
          return false;
        }
      ++bi;
    }
  return true;
}
///////////////////////////////////////////////////////////////////////////////
template<typename T>
inline
std::ostream &
operator<<(std::ostream &out, const std::vector<T> &v) {
  out << "std::vector(";
  for(const auto &t: v) { out << " " << t; }
  out << ")";
  return out;
}
///////////////////////////////////////////////////////////////////////////////
namespace {
  template<typename M, typename U, typename T>
  void
  do_test(M & marshal, U & unmarshal, const T &t1) {
    T t2;

    marshal   & t1;
    unmarshal & t2;

    std::cerr << "t1: " << t1 << std::endl;
    std::cerr << "t2: " << t2 << std::endl;
    std::cerr << "success: " << (t1 == t2) << std::endl;
    if(t1 != t2) {
        throw(rpcxx11::exceptions::invalid("t1 doesn't match t2"));
      }
  }
}
///////////////////////////////////////////////////////////////////////////////
int
main(int, char **) {
  std::vector<char> buffer(1024 * 1024);

  // coord
  MARKER("coord")
  {
    std::cerr << "testing coord..." << std::endl;
    rpcxx11::modules::binary::serialize::marshal_type<rpcxx11::delegate::data_view<char>>   marshal(buffer);
    rpcxx11::modules::binary::serialize::unmarshal_type<rpcxx11::delegate::data_view<char>> unmarshal(buffer);

    do_test(marshal, unmarshal, test::coord<int>(1, 2, 3));
  }

  // person
  MARKER("coord")
  {
    std::cerr << "testing person..." << std::endl;
    rpcxx11::modules::binary::serialize::marshal_type<rpcxx11::delegate::data_view<char>>   marshal(buffer);
    rpcxx11::modules::binary::serialize::unmarshal_type<rpcxx11::delegate::data_view<char>> unmarshal(buffer);

    do_test(marshal, unmarshal, test::person("Apu", "Nahasapeemapetilon"));
  }

  // combined
  MARKER("combined")
  {
    std::cerr << "testing combinations..." << std::endl;
    rpcxx11::modules::binary::serialize::marshal_type<rpcxx11::delegate::data_view<char>>   marshal(buffer);
    rpcxx11::modules::binary::serialize::unmarshal_type<rpcxx11::delegate::data_view<char>> unmarshal(buffer);

    do_test(marshal, unmarshal, 13);
    do_test(marshal, unmarshal, test::person("Apu", "Nahasapeemapetilon"));
    do_test(marshal, unmarshal, std::vector<uint32_t>({1, 3, 5, 7, 9, 11}));
    do_test(marshal, unmarshal, std::vector<test::person>({test::person("John", "One"),
                                                           test::person("John", "Two"),
                                                           test::person("John", "Six")}));
    do_test(marshal, unmarshal, 13.13);
  }
  // unordered
  MARKER("custom")
  {
    double       storage;
    test::writer w(storage);
    test::reader r(storage);

    rpcxx11::modules::binary::serialize::marshal_type<test::writer>   marshal(w);
    rpcxx11::modules::binary::serialize::unmarshal_type<test::reader> unmarshal(r);

    do_test(marshal, unmarshal, test::number(123));
  }
}
