// system
#include <iostream>
#include <memory>
#include <type_traits>
// rpcxx11
#include <rpcxx11/utils/sfinae.h>
///////////////////////////////////////////////////////////////////////////////
namespace {
  class one {
  public:
    void call(int n) const {
      std::cerr << __PRETTY_FUNCTION__ << "(" << n << ")" << std::endl;
    };
  };
  ///////////////////////////////////////////////////////////////////////////////
  class two {
  public:
    void call() const {
      std::cerr << __PRETTY_FUNCTION__ << "()" << std::endl;
    };
  };
  ///////////////////////////////////////////////////////////////////////////////
  class three {
  public:
  };
  ///////////////////////////////////////////////////////////////////////////////
  class four {
  public:
    template<typename T>
    void call(const T & t) {
      std::cerr << __PRETTY_FUNCTION__ << "(" << t << ")" << std::endl;
    }
  };
}
///////////////////////////////////////////////////////////////////////////////
int main(int, char **) {
  std::cerr << "one.... " << std::endl;
  one   obj_one;
  rpcxx11::utils::sfinae::if_exists::call(obj_one, 1);
  rpcxx11::utils::sfinae::if_exists::call(obj_one);

  std::cerr << "two.... " << std::endl;
  two   obj_two;
  rpcxx11::utils::sfinae::if_exists::call(obj_two, 2);
  rpcxx11::utils::sfinae::if_exists::call(obj_two);

  std::cerr << "two*... " << std::endl;
  std::shared_ptr<two> obj_two_ptr(std::make_shared<two>());
  rpcxx11::utils::sfinae::if_exists::call(obj_two_ptr, 2);
  rpcxx11::utils::sfinae::if_exists::call(obj_two_ptr);

  std::cerr << "three.. " << std::endl;
  three obj_three;
  rpcxx11::utils::sfinae::if_exists::call(obj_three, 13);
  rpcxx11::utils::sfinae::if_exists::call(obj_three);

  std::cerr << "four... " << std::endl;
  four obj_four;
  rpcxx11::utils::sfinae::if_exists::call(obj_four, std::string("hello, four"));
  rpcxx11::utils::sfinae::if_exists::call(obj_four);

  return 0;
}
